package ai.maum.m2u.service;

//import ai.maum.m2u.config.PropertyManager;
//import ai.maum.m2u.service.data.CheckCEKAccessTokenReq;
//import ai.maum.m2u.service.data.CheckCEKAccessTokenResp;
//import ai.maum.m2u.service.data.GetAuthorizationCodeResp;
//import ai.maum.m2u.service.data.GetCEKAccessTokenCheckResp;
import ai.maum.m2u.test.TestFinalValue;
//import com.google.gson.Gson;
import com.google.protobuf.ListValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.map.Authentication.*;
import maum.m2u.map.AuthenticationProviderGrpc.AuthenticationProviderImplBase;
//import org.apache.http.Header;
//import org.apache.http.entity.ContentType;
//import org.apache.http.message.BasicHeader;
//import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.util.ArrayList;
//import java.util.List;

public class AuthenticationImpl extends AuthenticationProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthenticationImpl.class);


  @Override
  public void signIn(SignInPayload request, StreamObserver<SignInResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signIn");

    try {
      SignInResultPayload.Builder signinResultPayload = SignInResultPayload.newBuilder();

      if (TestFinalValue.FIX_USER_KEY.equals(request.getUserkey()) &&
          TestFinalValue.FIX_USER_PASS.equals(request.getPassphrase())) {
        signinResultPayload.setAuthSuccess(
            AuthTokenPayload.newBuilder()
                .setAuthToken(TestFinalValue.FIX_AUTH_TOKEN)
                .setMeta(
                    Struct.newBuilder().putFields("data1",
                        Value.newBuilder().setStringValue("value1").build())
                ));
      } else {
        signinResultPayload.setAuthFailure(
            AuthFailurePayload.newBuilder()
                .setResCode("1111")
                .setMessage("Fail signIn :: Please Check userKey or passphrase")
                .setDetailMessage("detail message")
                .build());
      }

      responseObserver.onNext(signinResultPayload.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void multiFactorVerify(MultiFactorVerifyPayload request,
      StreamObserver<SignInResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signIn");

    try {
      SignInResultPayload.Builder signinResultPayload = SignInResultPayload.newBuilder();
      signinResultPayload.setAuthSuccess(
          AuthTokenPayload.newBuilder()
              .setAuthToken(TestFinalValue.FIX_AUTH_TOKEN)
              .setMultiFactor(true)
              .setMeta(
                  Struct.newBuilder().putFields("data1",
                      Value.newBuilder().setStringValue("value1").build())
              ))
          .build();
      responseObserver.onNext(signinResultPayload.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void signOut(SignOutPayload request,
      StreamObserver<SignOutResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signOut");

    try {
      responseObserver.onNext(null);
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateUserSettings(UserSettings request,
      StreamObserver<UserSettings> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.updateUserSettings");

    try {

      UserSettings.Builder userSettings = UserSettings.newBuilder();

      if (TestFinalValue.FIX_AUTH_TOKEN.equals(request.getAuthToken())) {
        // echo로 구현
        userSettings.setAuthToken(request.getAuthToken()).setSettings(request.getSettings());
      } else {
        throw new Exception("Un Valid Auth Token");
      }

      responseObserver.onNext(userSettings.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getUserSettings(UserKey request, StreamObserver<UserSettings> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.getUserSettings");

    try {

      UserSettings.Builder userSettings = UserSettings.newBuilder();

      // 임의의 데이터 return
      Struct.Builder sturct = Struct.newBuilder();
      sturct.putFields("userkey", Value.newBuilder().setStringValue("getUserSettings").build());
      sturct.putFields("passphrase", Value.newBuilder().setStringValue("1234").build());
      sturct.putFields("authParams", Value.newBuilder().setListValue(ListValue.newBuilder()).build());

      userSettings.setAuthToken(request.getAuthToken()).setSettings(sturct);

      responseObserver.onNext(userSettings.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

//  @Override
//  public void getAuthorizationCode(LGUAuthReq request, StreamObserver<LGUAuthResp> responseObserver) {
//    logger.info("===== Grpc Call :: AuthorizationImpl.getAuthorizationCode");
//
//    try {
//      String url = PropertyManager.getString("lgu.auth.url")+"/auth/getAuthorizationCode";
//      List<Header> headerList = new ArrayList<>();
//      headerList.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
//      headerList.add(new BasicHeader("svrKey", request.getHeader().getSvrKey()));
//      headerList.add(new BasicHeader("transactionid", request.getHeader().getTransactionid()));
//      headerList.add(new BasicHeader("messageid", request.getHeader().getMessageid()));
//      Header[] headers = headerList.toArray(new Header[headerList.size()]);
//
//      LGUAuthReqParam params = request.getParams();
//      Gson gson = new Gson();
//      String jsonString = gson.toJson(params);
//
//      String resp = HttpConnectionManager.getInstance().post(url, jsonString, headers);
//      logger.debug("getAuthorizationCode/out:{}", resp);
//      GetAuthorizationCodeResp obj = gson.fromJson(resp, GetAuthorizationCodeResp.class);
//
//      LGUAuthResp.Builder grpcOut = LGUAuthResp.newBuilder();
//      LGUAuthRespCommon.Builder outComm = LGUAuthRespCommon.newBuilder();
//      outComm.setResultCode(obj.getCommon().getResultCode());
//      outComm.setResultMessage(obj.getCommon().getResultMessage());
//
//      LGUAuthRespBody.Builder outBody = LGUAuthRespBody.newBuilder();
//      Cek.Builder cek = Cek.newBuilder();
//      cek.setAuthorizationCd(obj.getBody().getCek().getAuthorizationCd());
//      outBody.setCek(cek);
//      grpcOut.setRespComm(outComm.build());
//      grpcOut.setRespBody(outBody.build());
//
//      responseObserver.onNext(grpcOut.build());
//      responseObserver.onCompleted();
//
//    }catch(Exception e){
//      logger.error("{} => ", e.getMessage(), e);
//      responseObserver.onError(
//          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e))
//      );
//    }
//  }
//
//  @Override
//  public void getCEKAccessTokenCheck(LGUCekTokencheckReq request, StreamObserver<LGUCekTokencheckResp> responseObserver) {
//    logger.info("===== Grpc Call :: AuthorizationImpl.getCEKAccessTokenCheck");
//
//    try {
//      String url = PropertyManager.getString("lgu.auth.url")+"/auth/getCEKAccessTokenCheck";
//      List<Header> headerList = new ArrayList<>();
//      headerList.add(new BasicHeader("Content-Type", ContentType.APPLICATION_FORM_URLENCODED.getMimeType()));
//      Header[] headers = headerList.toArray(new Header[headerList.size()]);
//
//      ArrayList<BasicNameValuePair> params = new ArrayList<>();
//      params.add(new BasicNameValuePair("grant_type", request.getGrantType()));
//      params.add(new BasicNameValuePair("code", request.getCode()));
//      params.add(new BasicNameValuePair("redirect_uri", request.getRedirectUri()));
//      params.add(new BasicNameValuePair("client_id", request.getClientId()));
//      params.add(new BasicNameValuePair("refresh_token", request.getRefreshToken()));
//      params.add(new BasicNameValuePair("scope", request.getRefreshToken()));
//
//      Gson gson = new Gson();
//      String resp = HttpConnectionManager.getInstance().post(url, params, headers);
//      logger.debug("getCEKAccessTokenCheck/out:{}", resp);
//      GetCEKAccessTokenCheckResp obj = gson.fromJson(resp, GetCEKAccessTokenCheckResp.class);
//
//      LGUCekTokencheckResp.Builder grpcOut = LGUCekTokencheckResp.newBuilder();
//      grpcOut.setAccessToken(obj.getAccess_token());
//      grpcOut.setRefreshToken(obj.getRefresh_token());
//      grpcOut.setExpireIn(obj.getExpire_in());
//      grpcOut.setTokenType(obj.getToken_type());
//      grpcOut.setRedirectUri(obj.getRedirect_uri());
//      grpcOut.setClientId(obj.getClient_id());
//
//      responseObserver.onNext(grpcOut.build());
//      responseObserver.onCompleted();
//
//    }catch(Exception e){
//      logger.error("{} => ", e.getMessage(), e);
//      responseObserver.onError(
//              new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e))
//      );
//    }
//  }
//
//  @Override
//  public void checkCEKAccessToken(LGUCheckCekTokenReq request, StreamObserver<LGUCheckCekTokenResp> responseObserver) {
//    logger.info("===== Grpc Call :: AuthorizationImpl.checkCEKAccessToken");
//
//    try {
//      String url = PropertyManager.getString("lgu.auth.url") + "/auth/checkCEKAccessToken";
//      List<Header> headerList = new ArrayList<>();
//      headerList.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
//      headerList.add(new BasicHeader("svrKey", request.getHeader().getSvrKey()));
//      headerList.add(new BasicHeader("transactionid", request.getHeader().getTransactionid()));
//      headerList.add(new BasicHeader("messageid", request.getHeader().getMessageid()));
//      Header[] headers = headerList.toArray(new Header[headerList.size()]);
//
//      LGUCheckCekTokenReqParam params = request.getParams();
//      CheckCEKAccessTokenReq.Params mapParam = HttpRequestHelper.mapToCheckCEKAccessTokenReq_Params(params);
//      Gson gson = new Gson();
//      String jsonString = gson.toJson(mapParam);
//
//      String resp = HttpConnectionManager.getInstance().post(url, jsonString, headers);
//      logger.debug("checkCEKAccessToken/out:{}", resp);
//      CheckCEKAccessTokenResp obj = gson.fromJson(resp, CheckCEKAccessTokenResp.class);
//
//      LGUCheckCekTokenResp.Builder grpcOut = LGUCheckCekTokenResp.newBuilder();
//
//      LGUAuthRespCommon.Builder comm = LGUAuthRespCommon.newBuilder();
//      if(obj.getCommon().getResultCode() != null) {
//        comm.setResultCode(obj.getCommon().getResultCode());
//      }
//      if(obj.getCommon().getResultMessage() != null) {
//        comm.setResultMessage(obj.getCommon().getResultMessage());
//      }
//
//      LGUCheckCekTokenRespBody.Builder body = LGUCheckCekTokenRespBody.newBuilder();
//      if(obj.getBody().getDeviceToken() != null) {
//        body.setDeviceToken(obj.getBody().getDeviceToken());
//      }
//      if(obj.getBody().getDeviceId() != null) {
//        body.setDeviceId(obj.getBody().getDeviceId());
//      }
//      if(obj.getBody().getCustomId() != null) {
//        body.setCustomId(obj.getBody().getCustomId());
//      }
//      if(obj.getBody().getOneId() != null) {
//        body.setOneId(obj.getBody().getOneId());
//      }
//      if(obj.getBody().getSvcType() != null) {
//        body.setSvcType(obj.getBody().getSvcType());
//      }
//      if(obj.getBody().getDevType() != null) {
//        body.setDevType(obj.getBody().getDevType());
//      }
//
//
//      for(int i = 0;i<obj.getBody().getAuthInfos().size();i++){
//        CheckCEKAccessTokenResp.AuthInfo aInfo = obj.getBody().getAuthInfos().get(i);
//
//        LGUAuthInfo.Builder authinfo = LGUAuthInfo.newBuilder();
//        if(aInfo.getContSvcCd()!=null) {
//          authinfo.setContSvcCd(aInfo.getContSvcCd());
//        }
//        if(aInfo.getLogoutYn() != null) {
//          authinfo.setLogoutYn(aInfo.getLogoutYn());
//        }
//        if(aInfo.getUserContUseYn() != null) {
//          authinfo.setUserContUseYn(aInfo.getUserContUseYn());
//        }
//        if(aInfo.getSvcContGrantUseYn() != null) {
//          authinfo.setSvcContGrantUseYn(aInfo.getSvcContGrantUseYn());
//        }
//        if(aInfo.getSvcContUseYn() != null) {
//          authinfo.setSvcContUseYn(aInfo.getSvcContUseYn());
//        }
//        if(aInfo.getSvcType() != null) {
//          authinfo.setSvcType(aInfo.getSvcType());
//        }
//        if(aInfo.getDevType() != null) {
//          authinfo.setDevType(aInfo.getDevType());
//        }
//        if(aInfo.getAuthParameter1() != null) {
//          authinfo.setAuthParameter1(aInfo.getAuthParameter1());
//        }
//        if(aInfo.getAuthParameter2() != null) {
//          authinfo.setAuthParameter2(aInfo.getAuthParameter2());
//        }
//        if(aInfo.getAuthParameter3() != null) {
//          authinfo.setAuthParameter3(aInfo.getAuthParameter3());
//        }
//        if(aInfo.getAuthParameter4() != null) {
//          authinfo.setAuthParameter4(aInfo.getAuthParameter4());
//        }
//        if(aInfo.getAuthParameter5() != null) {
//          authinfo.setAuthParameter5(aInfo.getAuthParameter5());
//        }
//        if(aInfo.getAuthParameter6() != null) {
//          authinfo.setAuthParameter6(aInfo.getAuthParameter6());
//        }
//        if(aInfo.getAuthParameter7() != null) {
//          authinfo.setAuthParameter7(aInfo.getAuthParameter7());
//        }
//        if(aInfo.getAuthParameter8() != null) {
//          authinfo.setAuthParameter8(aInfo.getAuthParameter8());
//        }
//        if(aInfo.getAuthParameter9() != null) {
//          authinfo.setAuthParameter9(aInfo.getAuthParameter9());
//        }
//        if(aInfo.getAuthParameter10() != null) {
//          authinfo.setAuthParameter10(aInfo.getAuthParameter10());
//        }
//        body.addAuthinfos(authinfo.build());
//      }
//
//      grpcOut.setCommon(comm.build());
//      grpcOut.setBody(body.build());
//
//      responseObserver.onNext(grpcOut.build());
//      responseObserver.onCompleted();
//    }catch(Exception e){
//      logger.error("{} => ", e.getMessage(), e);
//      responseObserver.onError(
//              new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e))
//      );
//    }
//
//  }
}
