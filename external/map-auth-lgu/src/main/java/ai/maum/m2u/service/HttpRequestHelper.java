package ai.maum.m2u.service;

import ai.maum.m2u.service.data.CheckCEKAccessTokenReq;
import maum.m2u.map.Authentication;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class HttpRequestHelper {

    private static HttpRequestHelper instance = null;

    //for request cache
//    private HashMap<String, HttpGet> mapHttpGet = new HashMap<String, HttpGet>();
//    private HashMap<String, HttpPost> mapHttpPost = new HashMap<String, HttpPost>();
//    private HashMap<String, HttpPut> mapHttpPut = new HashMap<String, HttpPut>();

    public static HttpRequestHelper getInstance(){
        if(instance == null){
            instance = new HttpRequestHelper();
        }
        return instance;
    }

    private HttpRequestHelper() {

    }

    public HttpGet httpGet(String url) {

//		HttpGet get = mapHttpGet.get(url);
        HttpGet get = null;
        if(get == null){
            get = new HttpGet(url);
//			mapHttpGet.put(url, get);
        }
        return get;
    }

    public HttpPost httpPost(String url) {

//		HttpPost post = mapHttpPost.get(url);
        HttpPost post = null;
        if(post == null){
            post = new HttpPost(url);
//			mapHttpPost.put(url, post);
        }
        return post;
    }

    public HttpPut httpPut(String url) {

//		HttpPut put = mapHttpPut.get(url);
        HttpPut put = null;
        if(put == null){
            put = new HttpPut(url);
//			mapHttpPut.put(url, put);
        }
        return put;
    }

    public static CheckCEKAccessTokenReq mapToCheckCEKAccessTokenReq(Authentication.LGUCheckCekTokenReq request){

        CheckCEKAccessTokenReq req = new CheckCEKAccessTokenReq();

        Authentication.LGUAuthReqHeader header = request.getHeader();
        if(header != null){
            CheckCEKAccessTokenReq.ReqHeader mapHeader = new CheckCEKAccessTokenReq.ReqHeader();
            mapHeader.setCustomId(header.getCustomId());
            mapHeader.setMessageid(header.getMessageid());
            mapHeader.setSvrKey(header.getSvrKey());
            mapHeader.setTransactionid(header.getTransactionid());
            req.setHeader(mapHeader);
        }
        Authentication.LGUCheckCekTokenReqParam params = request.getParams();
        CheckCEKAccessTokenReq.Params mapParam = mapToCheckCEKAccessTokenReq_Params(params);
        req.setParams(mapParam);
        return req;
    }

    public static CheckCEKAccessTokenReq.Params mapToCheckCEKAccessTokenReq_Params(Authentication.LGUCheckCekTokenReqParam params){
        CheckCEKAccessTokenReq.Params mapParams = new CheckCEKAccessTokenReq.Params();
        if(params != null){

            if(params.getCommon() != null) {
                CheckCEKAccessTokenReq.Common common = new CheckCEKAccessTokenReq.Common();
                common.setOsInfo(params.getCommon().getOsInfo());
                common.setNwInfo(params.getCommon().getNwInfo());
                common.setCarrierType(params.getCommon().getCarrierType());
                common.setClientIp(params.getCommon().getClientIp());
                common.setDevInfo(params.getCommon().getDevInfo());
                common.setDevModel(params.getCommon().getDevModel());
                mapParams.setCommon(common);
            }
            if(params.getBody() != null){
                CheckCEKAccessTokenReq.Body body = new CheckCEKAccessTokenReq.Body();
                body.setServiceType(params.getBody().getServiceType());
                if(params.getBody().getCek() != null){
                    CheckCEKAccessTokenReq.Cek cek = new CheckCEKAccessTokenReq.Cek();
                    cek.setAccessToken(params.getBody().getCek().getAccessToken());
                    cek.setDeviceId(params.getBody().getCek().getDeviceId());
                    cek.setExtentionId(params.getBody().getCek().getExtentionId());
                    body.setCek(cek);
                }
                mapParams.setBody(body);
            }
        }
        return mapParams;
    }
}
