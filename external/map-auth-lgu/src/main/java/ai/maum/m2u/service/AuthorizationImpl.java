package ai.maum.m2u.service;

import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.service.data.CheckCEKAccessTokenReq;
import ai.maum.m2u.service.data.CheckCEKAccessTokenResp;
import ai.maum.m2u.service.data.LGUAuthSimpleData;
import ai.maum.m2u.test.TestFinalValue;
import com.google.gson.Gson;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.common.UserOuterClass.User.Builder;
import maum.m2u.map.Authentication;
import maum.m2u.map.Authentication.GetUserInfoRequest;
import maum.m2u.map.Authentication.GetUserInfoResponse;
import maum.m2u.map.Authentication.IsValidRequest;
import maum.m2u.map.Authentication.IsValidResponse;
import maum.m2u.map.AuthorizationProviderGrpc.AuthorizationProviderImplBase;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AuthorizationImpl extends AuthorizationProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthorizationImpl.class);

  public static final String svrKey = PropertyManager.getString("svrKey");//TODO fixed?
  public static final String serviceType = "SVC_001";

  @Override
  public void isValid(IsValidRequest request, StreamObserver<IsValidResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.isValid");

    try {
      IsValidResponse.Builder isValidResponse = IsValidResponse.newBuilder();
      String reqString = request.getAuthToken();
      logger.debug("isValid/token:{}", reqString);
      Gson gson = new Gson();
      LGUAuthSimpleData req = gson.fromJson(reqString, LGUAuthSimpleData.class);

      LGUAuthSimpleData.Common comm = new LGUAuthSimpleData.Common();
      comm.setClientIp(PropertyManager.getString("test.common.clientIp"));
      comm.setDevInfo(PropertyManager.getString("test.common.devInfo"));
      comm.setOsInfo(PropertyManager.getString("test.common.osInfo"));
      comm.setNwInfo(PropertyManager.getString("test.common.nwInfo"));
      comm.setDevModel(PropertyManager.getString("test.common.devModel"));
      comm.setCarrierType(PropertyManager.getString("test.common.carrierType"));
      req.setCommon(comm);

      String url = PropertyManager.getString("lgu.auth.url");
      List<Header> headerList = new ArrayList<>();
      headerList.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
      headerList.add(new BasicHeader("svrKey", svrKey));
      headerList.add(new BasicHeader("transactionid", req.getTransactionId()));//TODO check if empty string is allowed?
      headerList.add(new BasicHeader("messageid", req.getMessageId())); //TODO check if empty string is allowed?
      Header[] headers = headerList.toArray(new Header[headerList.size()]);

      CheckCEKAccessTokenReq.Params mapParam = new CheckCEKAccessTokenReq.Params();
        CheckCEKAccessTokenReq.Common common = new CheckCEKAccessTokenReq.Common();
        if(req.getCommon() != null) {
          common.setDevModel(req.getCommon().getDevModel());
          common.setDevInfo(req.getCommon().getDevInfo());
          common.setClientIp(req.getCommon().getClientIp());
          common.setCarrierType(req.getCommon().getCarrierType());
          common.setNwInfo(req.getCommon().getNwInfo());
          common.setOsInfo(req.getCommon().getOsInfo());
          mapParam.setCommon(common);
        }

      CheckCEKAccessTokenReq.Body body = new CheckCEKAccessTokenReq.Body();
      body.setServiceType(serviceType);
      CheckCEKAccessTokenReq.Cek cek = new CheckCEKAccessTokenReq.Cek();
      cek.setExtentionId(req.getExtentionId());
      cek.setDeviceId(req.getDeviceId());
      cek.setAccessToken(req.getAccessToken());
      body.setCek(cek);
      mapParam.setBody(body);

      String jsonString = gson.toJson(mapParam);

      String resp = HttpConnectionManager.getInstance().post(url, jsonString, headers);
      logger.debug("isValid/out:{}", resp);
      CheckCEKAccessTokenResp obj = gson.fromJson(resp, CheckCEKAccessTokenResp.class);
      logger.debug("isValid/obj:{}", obj);


      if(obj != null && obj.getBody() != null) {
//        isValidResponse.setAccessToken(req.getAccessToken());
        logger.debug("CurrentTime:{}", System.currentTimeMillis());
        isValidResponse.setAccessToken(reqString);
        isValidResponse.setExpiredAt(Timestamps.fromMillis(System.currentTimeMillis()));






        //isValidResponse.setExpiredAt((Timestamps.fromMillis(
        //    System.currentTimeMillis() * 1000 + 600*1000));
//      1000isValidResponse.setIsValid("Y".equals(authInfo.getUserContUseYn()));//TODO check if it's correct property
      }else{
        throw new Exception(obj.getCommon().getResultCode()+","+ obj.getCommon().getResultMessage());
      }

      CheckCEKAccessTokenResp.AuthInfo authInfo = null;
      if(obj.getBody() != null && obj.getBody().getAuthInfos().size() > 0) {
        authInfo = obj.getBody().getAuthInfos().get(0);
        isValidResponse.setIsValid("Y".equals(authInfo.getUserContUseYn()));
        if(StringUtils.isEmpty(req.getAccessToken()) && !StringUtils.isEmpty(req.getDeviceId())){
          isValidResponse.setIsValid(true);
        }
      }

      responseObserver.onNext(isValidResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getUserInfo(GetUserInfoRequest request,
      StreamObserver<GetUserInfoResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.getUserInfo");

    try {

      GetUserInfoResponse.Builder getUserInfoResponse = GetUserInfoResponse.newBuilder();

//      if (TestFinalValue.FIX_ACCESS_TOKEN.equals(request.getAccessToken())) {
//        User.Builder user = User.newBuilder();
//        Struct.Builder struct = Struct.newBuilder();
//        String json = null;
//        json = JsonFormat.printer().includingDefaultValueFields().print(Empty.newBuilder());
//        JsonFormat.parser().ignoringUnknownFields().merge(json, struct);
//
//        user.setUserId(TestFinalValue.FIX_USER_KEY);
//        user.setName("mindsLab");
//        user.setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN);
//        user.setMeta(struct);
//
//        getUserInfoResponse.setUser(user);
//
//      } else {
//        throw new Exception("Un Valid Access Token");
//      }
      String reqString = request.getAccessToken();
      logger.debug("getUserInfo/token:{}", reqString);
      Gson gson = new Gson();
      LGUAuthSimpleData req = gson.fromJson(reqString, LGUAuthSimpleData.class);

      LGUAuthSimpleData.Common comm = new LGUAuthSimpleData.Common();
      comm.setClientIp(PropertyManager.getString("test.common.clientIp"));
      comm.setDevInfo(PropertyManager.getString("test.common.devInfo"));
      comm.setOsInfo(PropertyManager.getString("test.common.osInfo"));
      comm.setNwInfo(PropertyManager.getString("test.common.nwInfo"));
      comm.setDevModel(PropertyManager.getString("test.common.devModel"));
      comm.setCarrierType(PropertyManager.getString("test.common.carrierType"));
      req.setCommon(comm);

      String url = PropertyManager.getString("lgu.auth.url");
      List<Header> headerList = new ArrayList<>();
      headerList.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
      headerList.add(new BasicHeader("svrKey", svrKey));
      headerList.add(new BasicHeader("transactionid", req.getTransactionId()));//TODO check if empty string is allowed?
      headerList.add(new BasicHeader("messageid", req.getMessageId())); //TODO check if empty string is allowed?
      Header[] headers = headerList.toArray(new Header[headerList.size()]);

      CheckCEKAccessTokenReq.Params mapParam = new CheckCEKAccessTokenReq.Params();
      CheckCEKAccessTokenReq.Common common = new CheckCEKAccessTokenReq.Common();
      if(req.getCommon() != null) {
        common.setDevModel(req.getCommon().getDevModel());
        common.setDevInfo(req.getCommon().getDevInfo());
        common.setClientIp(req.getCommon().getClientIp());
        common.setCarrierType(req.getCommon().getCarrierType());
        common.setNwInfo(req.getCommon().getNwInfo());
        common.setOsInfo(req.getCommon().getOsInfo());
        mapParam.setCommon(common);
      }

      CheckCEKAccessTokenReq.Body body = new CheckCEKAccessTokenReq.Body();
      body.setServiceType(serviceType);
      CheckCEKAccessTokenReq.Cek cek = new CheckCEKAccessTokenReq.Cek();
      cek.setExtentionId(req.getExtentionId());
      cek.setDeviceId(req.getDeviceId());
      cek.setAccessToken(req.getAccessToken());
      body.setCek(cek);
      mapParam.setBody(body);

      String jsonString = gson.toJson(mapParam);

      String resp = HttpConnectionManager.getInstance().post(url, jsonString, headers);
      logger.debug("getUserInfo/out:{}", resp);
      CheckCEKAccessTokenResp obj = gson.fromJson(resp, CheckCEKAccessTokenResp.class);
      logger.debug("getUserInfo/obj:{}", obj);

      User.Builder user = User.newBuilder();
      user.setAccessToken(req.getAccessToken());
      user.setUserId(obj.getBody().getOneId());

      CheckCEKAccessTokenResp.AuthInfo authInfo = null;
      if(obj.getBody() != null && obj.getBody().getAuthInfos().size() > 0) {
        authInfo = obj.getBody().getAuthInfos().get(0);
        if(!"Y".equals(authInfo.getUserContUseYn())){
          if(StringUtils.isEmpty(req.getAccessToken()) && !StringUtils.isEmpty(req.getDeviceId())){
            //pass
          }else {
            throw new Exception("Invalid token/userContUseYn=" + authInfo.getUserContUseYn());
          }
        }
        try {
          Struct.Builder stb = Struct.newBuilder();
//          JsonFormat.parser().ignoringUnknownFields().merge(gson.toJson(obj.getBody().getAuthInfos().get(0)), stb);
//          JsonFormat.parser().ignoringUnknownFields().merge(gson.toJson(obj.getBody()), stb);
//          JsonFormat.parser().ignoringUnknownFields().merge(req.getTransactionId(), stb);//add transactionId
          CheckCEKAccessTokenResp.AuthInfoSimple simple = new CheckCEKAccessTokenResp.AuthInfoSimple();
          simple.setBody(obj.getBody());
          simple.setTransactionid(req.getTransactionId());
          JsonFormat.parser().ignoringUnknownFields().merge(gson.toJson(simple), stb);
          Struct st = stb.build();
          user.setMeta(st);
        }catch(Exception e){
          logger.debug("authInfo serialization error/", e.getMessage());
        }
      }else{
        throw new Exception("Invalid token");
      }
      getUserInfoResponse.setUser(user.build());

      responseObserver.onNext(getUserInfoResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }


}
