package ai.maum.m2u.config;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

public class PropertyManager {

  static final Logger logger = LoggerFactory.getLogger(PropertyManager.class);

  private static String absoluteMindsPath = "MAUM_ROOT";
  private static String absoluteEtcPath = "/etc/";
  private static String absoluteConfigFileName = "m2u.conf";
//  private static String absoluteConfigFileName = "auth.conf";

  private static PropertiesConfiguration propertyConfig = null;

  private static void load() {
    if (propertyConfig == null) {
      logger.debug("### service-admin config file load");
      propertyConfig = getConfig();
    }
  }

  private static PropertiesConfiguration getConfig() {
    PropertiesConfiguration config = null;
    String confPath = getSystemConfPath();

    logger.debug("---------------------------------------");
    logger.debug("getSystemConfPath :: {}", confPath);
    logger.debug("getSystemConfFileName :: {}", absoluteConfigFileName);
    logger.debug("---------------------------------------");

    try {
      config = new PropertiesConfiguration(confPath+absoluteConfigFileName);
    } catch (ConfigurationException e) {
      logger.error("{} => ", e.getMessage(), e);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return config;
  }

  private static String getSystemConfPath(){
    String path = System.getenv(absoluteMindsPath);
//    path = path == null? "/Users/jaeheoncho/Workspace/Mindslab/maum": path;
    path += absoluteEtcPath;
    return path;
  }

  public static String getString(String propertyName) {
    load();
    return propertyConfig.getString(propertyName);
  }

  public static String[] getStringArray(String propertyName) {
    load();
    return propertyConfig.getStringArray(propertyName);
  }

  public static int getInt(String propertyName) {
    load();
    return propertyConfig.getInt(propertyName);
  }

  public static double getDouble(String propertyName) {
    load();
    return propertyConfig.getDouble(propertyName);
  }

  public static long getLong(String propertyName) {
    load();
    return propertyConfig.getLong(propertyName);
  }

  public static float getFloat(String propertyName) {
    load();
    return propertyConfig.getFloat(propertyName);
  }

  public static BigDecimal getBigDecimal(String propertyName) {
    load();
    return propertyConfig.getBigDecimal(propertyName);
  }

  public static List<Object> getList(String propertyName) {
    load();
    return propertyConfig.getList(propertyName);
  }

  public static Boolean getBoolean(String propertyName) {
    load();
    return propertyConfig.getBoolean(propertyName);
  }


}

