package ai.maum.m2u.test;

import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.service.data.LGUAuthSimpleData;
import com.google.gson.Gson;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import junit.framework.Test;
import maum.m2u.map.Authentication;
import maum.m2u.map.Authentication.LGUCheckCekTokenReq;
import maum.m2u.map.AuthenticationProviderGrpc;
import maum.m2u.map.AuthorizationProviderGrpc;

import java.util.concurrent.TimeUnit;


/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class TestClient {

//    public AuthenticationProviderGrpc.AuthenticationProviderBlockingStub clinetStub;
    public AuthorizationProviderGrpc.AuthorizationProviderBlockingStub clientStub;
    public ManagedChannel channel;

    public TestClient(){

        channel = ManagedChannelBuilder.forAddress("localhost", 10100).usePlaintext(true).build();
//        clientStub = AuthenticationProviderGrpc.newBlockingStub(channel);
        clientStub = AuthorizationProviderGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException{
        channel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
    }

    public void testMethod() {
//        LGUCheckCekTokenReq.Builder req = LGUCheckCekTokenReq.newBuilder();
//        Authentication.LGUAuthReqHeader.Builder header = Authentication.LGUAuthReqHeader.newBuilder();
//        header.setCustomId("custid xxx");
//        header.setMessageid("msgid xxx");
//        header.setSvrKey("svrkey xxx");
//        header.setTransactionid("transactionid xxx");
//        req.setHeader(header.build());
//
//        Authentication.LGUCheckCekTokenReqParam.Builder reqParam = Authentication.LGUCheckCekTokenReqParam.newBuilder();
//        Authentication.LGUAuthReqCommon.Builder comm = Authentication.LGUAuthReqCommon.newBuilder();
//        comm.setCarrierType("CCC");
//        comm.setClientIp("client ip");
//        comm.setDevInfo("devInfo");
//        comm.setDevModel("dev model");
//        comm.setNwInfo("nw info");
//        comm.setOsInfo("os info");
//        reqParam.setCommon(comm.build());
//
//        Authentication.LGUCheckCekTokenReqBody.Builder body =Authentication.LGUCheckCekTokenReqBody.newBuilder();
//        body.setServiceType("SVC_001");
//        Authentication.CheckCek.Builder cek = Authentication.CheckCek.newBuilder();
//        cek.setExtentionId("extid..");
//        cek.setDeviceId("devid..");
//        cek.setAccessToken("token....");
//        body.setCek(cek.build());
//        reqParam.setBody(body.build());
//
//
//        try {
//            Authentication.LGUCheckCekTokenResp resp = this.clinetStub.checkCEKAccessToken(req.build());
//            System.out.println("out: \n"+resp);
//        }catch(Exception e){
//            System.out.println("error:" + e.getMessage());
//        }

        String deviceId = PropertyManager.getString("test.deviceId");
        String accessToken = PropertyManager.getString("test.accessToken");
        String extentionId = PropertyManager.getString("test.extentionId");
        String transactionId = PropertyManager.getString("test.transactionId");;
        String messageId = PropertyManager.getString("test.messageId");
        String common_clientIp = PropertyManager.getString("test.common.clientIp");;
        String common_devInfo = PropertyManager.getString("test.common.devInfo");
        String common_osInfo = PropertyManager.getString("test.common.osInfo");
        String common_nwInfo = PropertyManager.getString("test.common.nwInfo");
        String common_devModel = PropertyManager.getString("test.common.devModel");
        String common_carrierType = PropertyManager.getString("test.common.carrierType");

        Authentication.IsValidRequest.Builder param = Authentication.IsValidRequest.newBuilder();
//        String tokenString = "{ \"deviceId\":\""+deviceId+"\", \"accessToken\":\""+accessToken+"\", \"extentionId\":\""+extentionId+"\", \"transactionId\":\""+transactionId+"\", \"messageId\":\""+messageId
//                +"\"}";

        LGUAuthSimpleData data = new LGUAuthSimpleData();
        LGUAuthSimpleData.Common common = new LGUAuthSimpleData.Common();
        common.setCarrierType(common_carrierType);
        common.setClientIp(common_clientIp);
        common.setDevInfo(common_devInfo);
        common.setDevModel(common_devModel);
        common.setNwInfo(common_nwInfo);
        common.setOsInfo(common_osInfo);
        data.setCommon(common);
        data.setAccessToken(accessToken);
        data.setDeviceId(deviceId);
        data.setExtentionId(extentionId);

        Gson gson = new Gson();
        String tokenString = gson.toJson(data);
        System.out.println("input string:" + tokenString);
        param.setAuthToken(tokenString);

        Authentication.GetUserInfoRequest.Builder param2 = Authentication.GetUserInfoRequest.newBuilder();
        param2.setAccessToken(tokenString);

        try {
            Authentication.IsValidResponse resp = this.clientStub.isValid(param.build());
            System.out.println("out: \n"+resp);

            Authentication.GetUserInfoResponse resp2 = this.clientStub.getUserInfo(param2.build());
            System.out.println("out: \n"+resp2);
        }catch(Exception e){
            System.out.println("error:" + e.getMessage());
        }

    }

    public static void main(String[] args){
        System.out.println("gRPC Client TEST");
        TestClient client = new TestClient();
        client.testMethod();
        try {
            client.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
