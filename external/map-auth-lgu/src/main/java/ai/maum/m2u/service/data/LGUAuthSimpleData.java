package ai.maum.m2u.service.data;

import java.io.Serializable;

/**
 * Created by jaeheoncho on 2018. 5. 23..
 */
public class LGUAuthSimpleData implements Serializable{


//    private String svrKey;
    private String transactionId;
    private String messageId;

    private Common common;

    private String deviceId;
    private String accessToken;
    private String extentionId;

    public LGUAuthSimpleData(){}

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExtentionId() {
        return extentionId;
    }

    public void setExtentionId(String extentionId) {
        this.extentionId = extentionId;
    }

    public static class Common implements Serializable{

        private String  clientIp;
        private String  devInfo;
        private String  osInfo;
        private String  nwInfo;
        private String  devModel;
        private String  carrierType;

        public Common() {
        }

        public String getClientIp() {
            return clientIp;
        }

        public void setClientIp(String clientIp) {
            this.clientIp = clientIp;
        }

        public String getDevInfo() {
            return devInfo;
        }

        public void setDevInfo(String devInfo) {
            this.devInfo = devInfo;
        }

        public String getOsInfo() {
            return osInfo;
        }

        public void setOsInfo(String osInfo) {
            this.osInfo = osInfo;
        }

        public String getNwInfo() {
            return nwInfo;
        }

        public void setNwInfo(String nwInfo) {
            this.nwInfo = nwInfo;
        }

        public String getDevModel() {
            return devModel;
        }

        public void setDevModel(String devModel) {
            this.devModel = devModel;
        }

        public String getCarrierType() {
            return carrierType;
        }

        public void setCarrierType(String carrierType) {
            this.carrierType = carrierType;
        }
    }
}
