package ai.maum.m2u.service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class HttpConnectionManager {

    private static HttpConnectionManager instance = null;
    private static PoolingHttpClientConnectionManager connectionManager = null;
    private SSLContext sslContext;

    private HttpConnectionManager(){
        if (connectionManager == null) {
            connectionManager = new PoolingHttpClientConnectionManager();
            connectionManager.setMaxTotal(100);
            connectionManager.setDefaultMaxPerRoute(30);//QQQ option

            try {
                sslContext = new SSLContextBuilder()
                        .loadTrustMaterial(null, (certificate, authType) -> true).build();
            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (KeyStoreException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static HttpConnectionManager getInstance(){
        if(instance == null){
            instance = new HttpConnectionManager();
        }
        return instance;
    }

    public synchronized HttpClient getHttpClient() {

        //QQQ keepalive optional
//		return new DefaultHttpClient(connectionManager);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setConnectionManager(connectionManager)
//		        .setKeepAliveStrategy(new HttpShortKeepAliveStrategy())
                .setSSLContext(sslContext)
                .setSSLHostnameVerifier(new NoopHostnameVerifier())//ssl
                .build();

        return httpClient;
    }

    public PoolingHttpClientConnectionManager getConnectionManager(){
        return connectionManager;
    }

    public void abort(HttpRequestBase httpRequest) {
        if (httpRequest != null) {
            try {
                httpRequest.abort();
            } catch (Exception e) {

            }
        }
    }

    public void release(HttpResponse response) {
        if (response != null && response.getEntity() != null)
            EntityUtils.consumeQuietly(response.getEntity());
    }


    public String get(String url, Header[] headers) throws Exception{
        HttpClient httpClient = null;
        HttpGet getRequest = null;
        HttpResponse response = null;

        try {
            httpClient = HttpConnectionManager.getInstance().getHttpClient();
            //httpGet = new HttpGet(url);
            getRequest = HttpRequestHelper.getInstance().httpGet(url);
//			ResponseHandler<String> responseHandler = new BasicResponseHandler();
            if(headers != null){
                getRequest.setHeaders(headers);
            }

            response = httpClient.execute(getRequest);

            StatusLine statusLine = response.getStatusLine();
            // 에러 발생
            if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
                throw new Exception(statusLine.getStatusCode()+","+statusLine.getReasonPhrase());
            }
            // 성공
            else {
                String responseString = new BasicResponseHandler().handleResponse(response);
                return responseString;
            }
        } catch (ClientProtocolException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(getRequest);
            throw e;
        } catch (IOException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(getRequest);
            throw e;
        } finally {
            HttpConnectionManager.getInstance().release(response);
        }
    }

    public String post(String url, List<BasicNameValuePair> params, Header[] headers) throws Exception{

        HttpPost postRequest = HttpRequestHelper.getInstance().httpPost(url);

        postRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

        HttpClient httpClient = null;
        HttpResponse response = null;
        try{
            if(headers != null){
                postRequest.setHeaders(headers);
            }
            httpClient = HttpConnectionManager.getInstance().getHttpClient();
            response = httpClient.execute(postRequest);

            StatusLine statusLine = response.getStatusLine();
            // 에러 발생
            if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
                throw new Exception(statusLine.getStatusCode()+","+statusLine.getReasonPhrase());
            }
            // 성공
            else {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (ClientProtocolException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(postRequest);
            throw e;
        } catch (IOException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(postRequest);
            throw e;
        } finally {
            HttpConnectionManager.getInstance().release(response);
        }
    }


    public String post(String url, String jsonString, Header[] headers) throws Exception{

        HttpPost postRequest = HttpRequestHelper.getInstance().httpPost(url);

        StringEntity input = new StringEntity(jsonString,"UTF-8");
//		if(StringUtils.isNotEmpty(contentType)){
//			input.setContentType(contentType);
//		}
        postRequest.setEntity(input);

        HttpClient httpClient = null;
        HttpResponse response = null;
        try{
            if(headers != null){
                postRequest.setHeaders(headers);
            }
            httpClient = HttpConnectionManager.getInstance().getHttpClient();
            response = httpClient.execute(postRequest);

            StatusLine statusLine = response.getStatusLine();
            // 에러 발생
            if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
                throw new Exception(statusLine.getStatusCode()+","+statusLine.getReasonPhrase());
            }
            // 성공
            else {
//				String responseString = new BasicResponseHandler().handleResponse(response);
//                return responseString;
//				InputStream is = null;
//				BufferedReader buffer = null;
//				StringBuilder sb = new StringBuilder();
//				try{
//					is = response.getEntity().getContent();
//					buffer = new BufferedReader(new InputStreamReader(is,"UTF-8"));
//					String str = null;
//
//					while( (str = buffer.readLine()) != null ){
//						sb.append(str).append("\n");
//					}
//					is.close();
//				}catch(Exception e){
//					throw e;
//				}finally{
//					if(buffer != null){
//						buffer.close();
//					}
//					if(is != null){
//						is.close();
//					}
//				}
//				return sb.toString();
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (ClientProtocolException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(postRequest);
            throw e;
        } catch (IOException e) {
//			e.printStackTrace();
            HttpConnectionManager.getInstance().abort(postRequest);
            throw e;
        } finally {
            HttpConnectionManager.getInstance().release(response);
        }
    }

//	public String post(String url, Object obj, Header[] headers) throws Exception{
//		return post(url,JsonUtils.toJson(obj), headers);
//	}

//	public String put(String url, Object obj, Header[] headers) throws Exception{
//		//HttpPut putRequest = new HttpPut(url);
//		HttpPut putRequest = HttpRequestHelper.getInstance().httpPut(url);
//
//		StringEntity input = new StringEntity(JsonUtils.toJson(obj));
////		input.setContentType("application/json");
//		putRequest.setEntity(input);
//
//		HttpClient httpClient = null;
//		HttpResponse response = null;
//		try{
//			if(headers != null){
//				putRequest.setHeaders(headers);
//			}
//			httpClient = HttpConnectionManager.getInstance().getHttpClient();
//			response = httpClient.execute(putRequest);
//
//			StatusLine statusLine = response.getStatusLine();
//			// 에러 발생
//			if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
//				throw new Exception(statusLine.getStatusCode()+","+statusLine.getReasonPhrase());
//			}
//			// 성공
//			else {
//				String responseString = new BasicResponseHandler().handleResponse(response);
//                return responseString;
//			}
//		} catch (ClientProtocolException e) {
////			e.printStackTrace();
//			HttpConnectionManager.getInstance().abort(putRequest);
//			throw new BizChatbotHttpException(e);
//		} catch (IOException e) {
////			e.printStackTrace();
//			HttpConnectionManager.getInstance().abort(putRequest);
//			throw new BizChatbotHttpException(e);
//		} finally {
//			HttpConnectionManager.getInstance().release(response);
//		}
//	}


    public static class HttpShortKeepAliveStrategy implements ConnectionKeepAliveStrategy {

        /**
         *
         * @param response
         * @param context
         * @return
         */
        @Override
        public long getKeepAliveDuration(HttpResponse response, HttpContext context) {

            // Honor 'keep-alive' header
            HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            while (it.hasNext()) {
                HeaderElement he = it.nextElement();
                String param = he.getName();
                String value = he.getValue();
                if (value != null && param.equalsIgnoreCase("timeout")) {
                    try {
                        return Long.parseLong(value) * 100;
                    } catch (NumberFormatException ignore) {
                    }
                }
            }


            //HttpHost target = (HttpHost) context.getAttribute(HttpClientContext.HTTP_TARGET_HOST);
//	        if ("www.mydomain.com".equalsIgnoreCase(target.getHostName())) {
//
//	// Keep alive for 5 seconds only
//	            return 5 * 1000;
//	        } else {
//
//	// otherwise keep alive for 1 seconds
//	            return 1 * 1000;
//	        }
            return 5 * 1000;
        }

    }

    //QQQ no auth, form url encoded
    public static Header[] getHeader(String conType) throws Exception{

        ArrayList<Header> h = new ArrayList<>();
        if("form".equals(conType)) {
            h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_FORM_URLENCODED.getMimeType()));
        }else if("json".equals(conType)){
            h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
        }

        return h.toArray(new Header[h.size()]);
    }
}
