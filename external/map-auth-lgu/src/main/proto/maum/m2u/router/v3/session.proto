/**
 * maum.ai M2U ROUTER V3 이후 스펙에서 사용할 Session 정보를 정의합니다.
 *
 * 세션은 연속된 대화 집합을 의미하며
 * 연결된 장치당 하나의 세션이 만들어지게 됩니다.
 * 이 세션에는 사용자가 있고 디바이스가 또한 존재합니다.
 *
 * 세션에는 `chatbot`이 하니씩 매핑되게 됩니다.
 *
 * 각 세션별로 여러개의 대화 (Talk)가 유지됩니다.
 */
syntax = "proto3";

package maum.m2u.router.v3;

import "google/protobuf/duration.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/struct.proto";
import "maum/common/lang.proto";
import "maum/m2u/common/dialog.proto";
// import "maum/m2u/da/provider.proto";
// import "maum/m2u/server/cl.proto";
// import "maum/m2u/router/v3/router.proto";

// 세션 정보
//
// 세션에서 하지 말아야할 관리 대상
//  - 장치 정보는 매번 바뀌고 장치가 initiator 이므로 중간에 캐시하지 않는다.
//    대신 device_id는 검색을 위해서 유지 한다.
//  - 사용자 정보도 매번 바뀌고 MAP에서 새롭게 유지된다. 중간에 캐시하지 않는다.
//    대신 user_id를 유지한다.
message DialogSession {

  // ID: 세션을 구분짓는 유일한 정보들

  // ID
  // 세션별로 UNIQUE한 ID값 유지
  // IMDG 서버가 새로 시작하면 time_t 를 구하여 `<< 30` 처리한 값을 초기값을
  // 초기화한다.
  int64 id = 1;

  // 사용자 UNIQUE ID
  // 이 값은 IMDG에서 검색이 가능해야 한다.
  // INDEX로 걸려 있어야 한다.
  string user_id = 2;

  // 디바이스 UNIQUE ID
  // 이 값은 IMDG에서 검색이 가능해야 하며
  // INDEX로 걸려 있어야 한다.
  string device_id = 3;
  // 원격 PEER의 정보의 정보
  string peer = 4;

  // 대화 진행에 관련된 정보들

  // 현재의 챗봇 정보
  // 챗봇이 바뀌면 세션을 종료하고 새롭게 다시 만들어야 한다.
  // 챗봇은 세션 동안에만 유일하다.
  string chatbot = 11;

  // 이 세션에서 참조하는 INTENT FINDER POLICY의 이름
  // Router는 이 정책의 이름을 IMDG에서 query를 통해서 구해오고
  // round robin 방식으로 ITF에 접근할 수 있다.
  string intent_finder_policy_name = 12;

  // 대화 진행에 필요한 컨텍스트
  // 이 컨텍스트는 대화가 종료하면 동시에 사려져야 한다.
  // 참고로 meta는 매 대화마다 새롭게 기록되는 정보이므로
  // 로컬 변수 및 Talk에 유지되어야 한다.
  google.protobuf.Struct context = 13;


  // 마지막에 사용한 대화 skill
  // 만일 대화 호출 후에 `closeSkill = true`인 조건이 발생하게 되면
  // 이 값은 비어있어야 한다. 그래야만 다음 대화에서 ITF를 호출하여 새로운 스킬을
  // 찾을 수 있습니다.
  string last_skill = 21;

  // 현재 대화와 관련된 current agent
  // 대화 에이젠트 인스턴스의 리소스 키, UUID로서
  // 대화 에이전트를 구분해주는 하나의 키이다.
  // 멀티턴 대화의 경우에는 이 키를 사용해서 이전 대화를 찾아야 합니다.
  // 이전 대화를 찾을 때 last_agent_key가 사라지고 없을 경우에는
  // 같은 서비스를 제공하는 다른 DAIR를 찾아야 합니다.
  // 이때 (chatbot, skill, lang) 튜플을 사용해야 합니다.
  string last_agent_key = 22;

  // 세션의 상태

  // 세션을 무효화 시킨다.
  // 관리자에 의해서 이 세션을 강제로 무효화 할 수 있다.
  // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
  bool valid = 31;

  // 현재 대화가 진행 중인지를 표시한다.
  // Router는 내부적으로 매 대화마다 소요시간을 측정하도록 하고
  // 측정을 시작할 때 talking을 켜고, 응답을 내보내면 끈다.
  bool talking = 32;

  //
  // TODO: CONFINENCE LEVEL 정보는 추후에 더 구체적으로 정의한다.
  // TODO: dilaog trigger 기능 은 ARCHITECTURE 재구성 후 구현

  // 대화 통게 관련 데이터

  // 누적된 대화 시간을 기록합니다.
  // 여기에는 실지로 대화 호출에 사용된 누적된 시간입니다.
  // 함수 호출, 응답대기를 포함한 시간입니다.
  google.protobuf.Duration accumulated_time = 101;

  // 처음 대화가 시작한 시간
  google.protobuf.Timestamp started_at = 102;

  // 마지막 대화 시간
  // 이 값은 세션 만료시간을 판단하는 데 중요한 기준이 된다.
  // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
  google.protobuf.Timestamp last_talked_at = 103;


  //
  // 문제가 있는 대화에 대한 관리 사항은 추후에 진행한다.
  // 그래서 여기에서 정의하지 않는다.
}



// 한턴의 대화에 대한 상세 기록
message SessionTalk {
  // 순서
  uint32 seq = 1;
  // DialogSession.id
  int64 session_id = 2;
  // 언어
  maum.common.Lang lang = 3;

  // 스킬
  string skill = 11;
  // 현재의 intent
  string intent = 12;
  // 현재 스킬의 마지막 대화인가?
  bool close_skill = 13;

  // 입력
  // 입력 텍스트
  string in = 21;
  // 입력 유형
  maum.m2u.common.Utter.InputType input_type = 22;
  // 입력 메타, IMAGE와 같은 데이터일 경우에 필요함.
  google.protobuf.Struct input_meta = 23;

  // 출력

  // 출력 텍스트
  string out = 31;
  // TTS로 나가는 내용이 다를 때
  string speech_out = 32;
  // 재질의 상태인 경우
  bool reprompt = 33;

  // 출력 메타 데이터
  map<string, string> output_meta = 41;

  // 출력 context 업데이트
  google.protobuf.Struct update_context = 42;

  // grpc 최종 status
  int32 status_code = 51;
  // 내부 처리 코드
  DialogResult dialog_result = 52;

  // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
  // 복수의 대화 에이전트 정보
  repeated TalkAgent agents = 61;

  // 시작 시간
  google.protobuf.Timestamp start = 71;
  // 끝 시간
  google.protobuf.Timestamp end = 72;
  // 각 구간별 시간
  map<string, google.protobuf.Duration> elapsed_time = 73;

  // *optional* 사용자 피드백
  // 사용자가 매기는 점수, 5점척도 또는 10점 척도, 2점 척도 등 다양하게 정의할 수 있다.
  // 매기지지 않으면 -1 이다.
  // 통계에서 뺀다.
  int32 feedback = 91;

  // *optional* 대화 정확도
  // 관리자가 큐레이션 과정에서 매기는 점수이다.
  int32 accuracy = 92;

  // *optional* 다른 챗봇으로 전달을 위한 챗봇 이름
  // FIXME: 다른 챗봇으로 이전하는 경우에만 사용
  string transit_chatbot = 101;
}


// 라우터의 대화 처리 결과
// 라우터 내부의 오류 코드
enum DialogResult {
  // 성공
  OK = 0;
  // 사용자가 무효화되는 경우, 없음, MAP
  USER_INVLIDATED = 101;
  // CHATBOT 발견되지 않음
  CHATBOT_NOT_FOUND = 102;
  // 세션이 없을 경우
  SESSION_NOT_FOUND = 103;
  // 세션이 무효화된 경우
  SESSION_INVLIDATED = 104;

  // 빈 발화
  EMPTY_IN = 201;

  // ITF 호출 실패
  INTENT_FINDER_FAILED = 301;
  // DA 없음
  DIALOG_AGENT_NOT_FOUND = 302;
  // ?????
  DIALOG_AGENT_NOT_READY = 303;
  // 마지막 DA가 없으면 바로 리컨했나?
  LAST_DIALG_AGENT_LOST = 304;
  // 같은 스킬을 또 찾음
  // goto 가 이전과 같음
  SECONDARY_DIALOG_CONFLICT = 311;
  // ??
  LAST_AGENT_NOT_FOUND = 312;
  // ??
  DUPLICATED_SKILL_RESOLVED = 313;
  // goto once or cannot understand의 경우에는 single turn 만
  SECONDARY_DIALOG_AGENT_IS_MULTI_TURN = 314;
  // ??
  INTERNAL_ERROR = 500;
}

/**
 *
 */
message TalkAgent {
  // 스킬
  string skill = 1;
  // 인텐트
  string intent = 2;
  // 에이전트 이름
  string agent = 3;
  // 언어
  maum.common.Lang lang = 7;

  // 이유
  TransionReason reason = 11;
  enum TransionReason {
    UNKNOWN = 0;
    OPEN = 1;
    USER_UTTER = 2;
    SKILL_TRANS_GOTO = 11;
    SKILL_TRANS_GOTO_ONCE = 12;
    SKILL_TRANS_GOTO_DEFAULT = 13;
    SKILL_TRANS_DISCARD = 21;
    SKILL_TRANS_CANNOT_UNDERSTAND = 22;
    CHATBOT_TRANS_GOTO = 31;
  }
}
