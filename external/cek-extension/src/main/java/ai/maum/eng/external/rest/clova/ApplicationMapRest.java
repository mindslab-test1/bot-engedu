package ai.maum.eng.external.rest.clova;

import ai.maum.eng.external.rest.clova.common.config.PropertyManager;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan
public class ApplicationMapRest extends SpringBootServletInitializer {
  static final org.slf4j.Logger logger = LoggerFactory
      .getLogger(ApplicationMapRest.class);
  public static void main(String[] args) {
    logger.debug("Server is started!");
    SpringApplication.run(ApplicationMapRest.class, args);

  }

  @Bean
  public EmbeddedServletContainerCustomizer containerCustomizer () {
    return (container -> {
      container.setSessionTimeout(PropertyManager.getInt("server.session.timeout"));
      logger.info("Server Connection Timeout");
    });
  }

}




