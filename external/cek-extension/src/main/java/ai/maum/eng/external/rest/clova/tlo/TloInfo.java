package ai.maum.eng.external.rest.clova.tlo;

public class TloInfo {

  public String seq_id;                     //1
  public String log_time;                   //2
  public String log_type;                   //3
  public String sid;                        //4
  public String result_code;                //5
  public String req_time;                   //6
  public String rsp_time;                   //7
  public String client_ip;                  //8
  public String dev_info;                   //9
  public String os_info;                    //10
  public String nw_info;                    //11
  public String svc_name;                   //12
  public String dev_model;                  //13
  public String carrier_type;               //14
  public String tr_id;                      //15
  public String msg_id;                     //16
  public String from_svc_name;              //17
  public String to_svc_name;                //18
  public String svc_type;                   //19
  public String dev_type;                   //20
  public String dev_token;                  //21
  public String intent_type;                //22
  public String intent_value;               //23
  public String stt_text;                   //24
  public String clovareq_id;                //25
  public String svc_code;                   //26

}
