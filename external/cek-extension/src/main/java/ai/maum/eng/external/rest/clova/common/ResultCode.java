package ai.maum.eng.external.rest.clova.common;

public enum ResultCode {
  //"{\"version\":\"0.1.0\",\"sessionAttributes\":{},\"response\":{\"outputSpeech\":{\"type\":\"SpeechList\",\"values\":[{\"type\":\"PlainText\",\"lang\":\"ko\",\"value\":\"What's your favorite animal?\"}]},\"card\":{},\"directives\":[],\"shouldEndSession\":true}}";
  SUCCESS(20000000, "none"),
  ClOVA_CODE_GUIDE(30010000, "not support"),
  REQUEST_STRANGE(30010001, "request is not valid"),
  MAP_CALL_ERROR(40001000,"map callEvent fail"),
  MAP_RETURN_NULL(40001001, "map return null"),
  LMS_SERVER_ERROR(40001002, "lms suerver error");

  private final int code;
  private final String description;

  private ResultCode(int code, String description) {
    this.code = code;
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public int getCode() {
    return code;
  }

  @Override
  public String toString() {
    return code + ": " + description;
  }
}
