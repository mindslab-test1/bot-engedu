package ai.maum.eng.external.rest.clova.tlo;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import java.nio.charset.Charset;

/**
 * MAP에서 TLOLogEncoder로 전달되는 정보는 Router연동(OPEN, TALK, CLOSE) 정보이다
 * 연동 해당 정보는 Arguments, MDC 를 이용하여 전달된다
 */
public class TLOLogEncoder extends LayoutWrappingEncoder<ILoggingEvent> {

  private Charset charset;

  /**
   * 문자열을 charset에 따라 byte[]로 변환한다.
   */
  private byte[] convertToBytes(String s) {
    if (charset == null) {
      return s.getBytes();
    } else {
      return s.getBytes(charset);
    }
  }

  /**
   * Encoder에 사용할 Character set 설정
   */
  @Override
  public void setCharset(Charset charset) {
    super.setCharset(charset);
    this.charset = getCharset();
  }

  /**
   * event로 전달된 Marker, Message, Arguments, MDC Map 등을 이용하여 로그를 조합한다.
   */
  @Override
  public byte[] encode(ILoggingEvent event) {

    // 변수 초기화
    StringBuffer buffer = new StringBuffer();
    TloInfo info = new TloInfo();

    for (Object object : event.getArgumentArray()) {
      if (object instanceof TloInfo) {
        info = (TloInfo)object;
      }
    }

    String msg = "";
    msg += "SEQ_ID=" + info.seq_id;
    msg += "|LOG_TIME=" + info.log_time;
    msg += "|LOG_TYPE=" + info.log_type;
    msg += "|SID=" + info.sid;
    msg += "|RESULT_CODE=" + info.result_code;
    msg += "|REQ_TIME=" + info.req_time;
    msg += "|RSP_TIME=" + info.rsp_time;
    msg += "|CLIENT_IP=" + info.client_ip;
    msg += "|DEV_INFO=" + info.dev_info;
    msg += "|OS_INFO=" + info.os_info;
    msg += "|NW_INFO=" + info.nw_info;
    msg += "|SVC_NAME=" + info.svc_name;
    msg += "|DEV_MODEL=" + info.dev_model;
    msg += "|CARRIER_TYPE=" + info.carrier_type;
    msg += "|TR_ID=" + info.tr_id;
    msg += "|MSG_ID=" + info.msg_id;
    msg += "|FROM_SVC_NAME=" + info.from_svc_name;
    msg += "|TO_SVC_NAME=" + info.to_svc_name;
    msg += "|SVC_TYPE=" + info.svc_type;
    msg += "|DEV_TYPE=" + info.dev_type;
    msg += "|DEVICE_TOKEN=" + info.dev_token;
    msg += "|INTENT_TYPE=" + info.intent_type;
    msg += "|INTENT_VALUE=" + info.intent_value;
    msg += "|STT_TEXT=" + info.stt_text;
    msg += "|CLOVA_REQ_ID=" + info.clovareq_id;
    msg += "|SVC_CODE=" + info.svc_code;
    msg += "\n";

    buffer.append(msg);

    return convertToBytes(buffer.toString());
  }

}