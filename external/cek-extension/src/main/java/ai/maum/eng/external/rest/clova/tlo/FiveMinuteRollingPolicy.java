package ai.maum.eng.external.rest.clova.tlo;

import static java.time.temporal.ChronoField.*;

import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;


/**
 * 5분 단위로 파일을 생성하는 Policy
 *
 * Timer를 이용하여 5분 주기로 rollover()를 수행하여 로그 출력이 없을 때도 로그 파일을 생성한다.
 */
public class FiveMinuteRollingPolicy extends TimeBasedRollingPolicy {

  private static final DateTimeFormatter fileDateHourFormatter = new DateTimeFormatterBuilder()
      .appendValue(YEAR, 4)
      .appendValue(MONTH_OF_YEAR, 2)
      .appendValue(DAY_OF_MONTH, 2)
      .appendValue(HOUR_OF_DAY, 2)
      .toFormatter();

  /**
   * rollover()를 수행할 RollingFileAppender
   */
  private TLOLogAppender appender;

  /**
   * 현재 기록 중인 5분단위 값
   */
  private int fiveMinuteStep;

  /**
   * 5분 주기 Trigger Timer
   */
  private Timer fiveMinuteTriggerTimer = new Timer();

  /**
   * 5분 주기 Trigger Timer를 설정한다.
   */
  private void setTriggerTimer() {
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        try {
          if (appender != null) {
            appender.performRollover();
          }
        } finally {
          setTriggerTimer();
        }
      }
    };

    Calendar next = new GregorianCalendar();
    next.setTimeInMillis(System.currentTimeMillis());
    next.set(Calendar.MINUTE, getCurrentFiveMinuteStep() + 5);
    next.set(Calendar.SECOND, 0);
    next.set(Calendar.MILLISECOND, 0);

    fiveMinuteTriggerTimer.schedule(task, next.getTime());
  }

  /**
   * rollover를 수행할 RollingFileAppender를 설정한다.
   */
  public void setRollingFileAppender(TLOLogAppender appender) {
    this.appender = appender;
  }

  /**
   * 5분단위로 로그 파일을 생성한다.
   */
  public int getCurrentFiveMinuteStep() {
    // 현재의 분을 구한다.
    int minutes = LocalDateTime.now().getMinute();

    // 현재의 분에서 뒷자리를 구한다.
    int trailing = minutes % 10;

    // 현재의 분에서 뒷자리를 없앤다.
    minutes -= trailing;
    // 뒷자리가 5보다 크거나 같으면
    if (trailing > 4) {
      // 5분 단위를 더한다.
      minutes += 5;
    }

    return minutes;
  }

  /**
   * 시작할 때, 5분단위 값을 초기화하고 Trigger Timer를 설정한다.
   */
  @Override
  public void start() {
    fiveMinuteStep = getCurrentFiveMinuteStep();
    setTriggerTimer();
    super.start();
  }

  /**
   * 중지할 할 때, Trigger Timer를 취소한다.
   */
  @Override
  public void stop() {
    fiveMinuteTriggerTimer.cancel();
    super.stop();
  }

  /**
   * 새로 생성할 로그 파일 이름을 가져온다.
   */
  @Override
  public String getActiveFileName() {
    String dateHourMin = LocalDateTime.now().format(fileDateHourFormatter)
        + String.format("%02d", fiveMinuteStep);
    String date = dateHourMin.substring(0, 8);

    return super.getFileNamePattern()
        .replace("%d{yyyyMMdd}", date)
        .replace("%d{yyyyMMddHHmm}", dateHourMin);
  }

  /**
   * 기존의 기록한 파일이 있을 때만 super.rollover()를 수행한다.
   *
   * 기존 파일이 없을 경우 Exception이 발생한다.
   */
  @Override
  public void rollover() {
    if (getTimeBasedFileNamingAndTriggeringPolicy().getElapsedPeriodsFileName() != null) {
      super.rollover();
    }
    // getActiveFileName() 호출 시 파일이름을 위해서 갱신한다.
    fiveMinuteStep = getCurrentFiveMinuteStep();
  }

}
