package ai.maum.eng.external.rest.clova.common;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


public class PayloadCheck {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(PayloadCheck.class);

  private static final String SUCCESS_CODE = "20000000";
  private static final String EVAL_INTERNAL_ERROR = "40001002";
  private static final String SDS_INTERNAL_ERROR = "40001003";
  private static final String LMS_INTERNAL_ERROR = "40001004";
  private static final String DB_INTERNAL_ERROR = "40001005";

  static public String getResultCode(Value metaResponse) {
    String result = "";
    Value payResMsg = metaResponse.getStructValue().getFieldsOrThrow("resultCode");
    try {
      result = JsonFormat.printer().print(payResMsg);
      logger.info(result);
      result = result.replaceAll("\"", "");
      logger.info(result);

      if(result.equals(SUCCESS_CODE)){
        logger.info("equal complete");
        result = SUCCESS_CODE;
      }
      else if (result.equals("42000001")
          || result.equals("42000002")
          || result.equals("42000003")){
        logger.error("EVAL Server Error");
        result = EVAL_INTERNAL_ERROR;
      }
      else if (result.equals("42000004")
          || result.equals("42000005")
          || result.equals("42000006")){
        logger.error("SDS Server Error");
        result = SDS_INTERNAL_ERROR;
      }
      else if (result.equals("42000007")){
        logger.error("LMS Server Error");
        result = "42000007";
      }
      else if (result.equals("42000008")
          || result.equals("42000009")){
        logger.error("LMS Server Error");
        result = LMS_INTERNAL_ERROR;
      }
      else if (result.equals("42000010")
          || result.equals("42000011")
          || result.equals("42000012")){
        logger.error("DB Server Error");
        result = DB_INTERNAL_ERROR;
      }

      return result;

    } catch (Exception e) {
      logger.debug("json parse error");
    }
    return result;

  }
}
