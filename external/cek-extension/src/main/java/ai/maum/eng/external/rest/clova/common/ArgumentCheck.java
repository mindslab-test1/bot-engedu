package ai.maum.eng.external.rest.clova.common;

import ai.maum.eng.external.rest.clova.controller.ClovaController;
import java.util.HashMap;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class ArgumentCheck {
  static final org.slf4j.Logger logger = LoggerFactory
      .getLogger(ArgumentCheck.class);

  private static final String EXTENSION_ID = "com.uplus.ucfc.lms.kidstalk";

  // CFC 테스트 결과중 '파싱에러 정상출력'건만 처리
  public boolean checkValid(HashMap<String, Object> param) {


//    HashMap<String, Object> context = null;

    HashMap<String, Object> context = null;
    HashMap<String, Object> System = null;
    HashMap<String, Object> application = null;
    HashMap<String, Object> user = null;
    HashMap<String, Object> device = null;


    // TST_E_001 : Request전문 중 context 키값 오류
    if (!param.containsKey("context")) {
      logger.info("Request전문 중 context 키값 오류 ");
      return false;
    }
    context = (HashMap<String, Object>) param.get("context");

    // TST_E_003 : Request전문 중 context.System 키값 오류
    if (!context.containsKey("System")) {
      logger.info("Request전문 중 context.System 키값 오류 ");
      return false;
    }
    System = (HashMap<String, Object>) context.get("System");

    // TST_E_005 :Request전문 중 context.System.application 키값 오류
    if (!System.containsKey("application")) {
      logger.info("Request전문 중 context.System.application 키값 오류 ");
      return false;
    }
    application = (HashMap<String, Object>) System.get("application");

    // TST_E_007 :Request전문 중 context.System.applcation.apllicationId 키값 오류
    if (!application.containsKey("applicationId")) {
      logger.info("Request전문 중 context.System.apllication.apllicationId 키값 오류");
      return  false;
    }

    // TST_E_009 :Request 전문 중 context.System.application.applicationId 내용이
    // com.uplus.ucfc.lms.kidstalk가 아닐 경우
    if (!EXTENSION_ID.equals(application.get("applicationId"))) {
      logger.info("Request 전문 중 "
          + "context.System.application.applicationId 내용이 com.uplus.ucfc.lms.kidstalk 아님");
      return  false;
    }

    // TST_E_010 :Request전문 중 context.System.user 키값 오류
    if (!System.containsKey("user")) {
      logger.info("Request 전문 중 context.System.user 항목제거");
      return false;
    }

    user = (HashMap<String, Object>) System.get("user");

    // TST_E_013 : Request전문 중 context.System.device key값 오류
    if (!System.containsKey("device")) {
      logger.info("Request전문 중 context.System.device 항목제거");
      return false;
    }
    device = (HashMap<String, Object>) System.get("device");

    // TST_E_015 : Request전문 중 context.System.device.deviceId key값 오류
    if (!device.containsKey("deviceId")) {
      logger.info("Request전문 중 context.System.device.deviceId key변조");
      return false;
    }

    // TST_E_016 : Request전문 중 context.System.device.deviceId 내용공백
    if (device.containsKey("deviceId")) {
      if ("".equals(device.get("deviceId")) || device.get("deviceId") == null) {
        logger.info("Request전문 중 context.System.device.deviceId 내용공백");
        return false;
      }
    }

    return true;
  }
}
