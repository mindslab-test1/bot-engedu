package ai.maum.eng.external.rest.clova.grpc;

import ai.maum.eng.external.rest.clova.common.config.PropertyManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import org.slf4j.LoggerFactory;


public class MapGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(MapGrpcInterfaceManager.class);

  private static final String M2U_AUTH_SIGN_IN_HEADER = "m2u-auth-sign-in";
  private static final String M2U_AUTH_TOKEN_HEADER = "m2u-auth-token";

  private String mapHost;
  private int mapPort;

  public MapGrpcInterfaceManager(String host, int port) {
    mapHost = host;
    mapPort = port;
  }

  /**
   * Map의 eventStream를 호출하는 서비스입니다.
   */
  public MapDirective callEvent(MapEvent req, String authToken) throws Exception {
    logger.trace("#@#@==========TestGrpcInterfaceManager.callEvent : mapHost[{}] mapPort[{}] req[{}]",
        mapHost, mapPort, req);

    ManagedChannel channel = ManagedChannelBuilder.forAddress(mapHost, mapPort)
        .usePlaintext()
        .build();

    try {
      //헤더가 중첩으로 쌓이는 현상이 발생하여 stub 초기화
      MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub maumToYouProxyServiceStub = MaumToYouProxyServiceGrpc
          .newStub(channel);
      // GRPC 헤더에  추가
      Metadata fixedHeaders = new Metadata();
      // Sigin Service 호출시 M2U_AUTH_SIGN_IN_HEADER가 추가되어야 합니다.
      Metadata.Key<String> signInkey = Metadata.Key
          .of(M2U_AUTH_SIGN_IN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> authTokenkey = null;

      if (!"".equals(authToken)) {
        // authToken토큰을 발급받은 이후에는 모든 Service 호출시 M2U_AUTH_TOKEN_HEADER에 토큰값을 할당해야 합니다.
        authTokenkey = Metadata.Key
            .of(M2U_AUTH_TOKEN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
        fixedHeaders.put(authTokenkey, authToken);
      }

      // M2U_AUTH_SIGN_IN_HEADER에 값이 없어도 되지만 OperationSyncId를 할당해 주었습니다.
      fixedHeaders.put(signInkey, req.getEvent().getOperationSyncId());
      maumToYouProxyServiceStub = MetadataUtils
          .attachHeaders(maumToYouProxyServiceStub, fixedHeaders);

      // 비동기 처리를 위해서 CountDownLatch를 사용하였습니다.
      final CountDownLatch finishLatch = new CountDownLatch(1);
      HashMap<String, MapDirective> result = new HashMap<>();

      StreamObserver<MapDirective> responseObserver = new StreamObserver<MapDirective>() {
        @Override
        public void onNext(MapDirective res) {
          result.put("result", res);
        }

        @Override
        public void onError(Throwable t) {
          finishLatch.countDown();
        }

        @Override
        public void onCompleted() {
          finishLatch.countDown();
        }
      };

      StreamObserver<MapEvent> requestObserver = maumToYouProxyServiceStub
          .eventStream(responseObserver);

      requestObserver.onNext(req);
      requestObserver.onCompleted();

      // finishLatch를 사용하여 최대 50초간 blocking 하도록 합니다.
      if (!finishLatch.await(PropertyManager.getInt("client.session.timeout"),
          TimeUnit.MILLISECONDS)) {
        logger.info("Client Connection TimeOut");
      }

      try {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      return result.get("result");
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      } finally {
        channel.shutdown();
        throw e;
      }
    } catch (InterruptedException e) {
      logger.error("{} => ", e.getMessage(), e);
      channel.shutdown();
      return null;
    }
  }
}
