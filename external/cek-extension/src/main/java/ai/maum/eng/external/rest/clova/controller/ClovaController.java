package ai.maum.eng.external.rest.clova.controller;

import static ai.maum.eng.external.rest.clova.tlo.TLOLogUtills.makeTLOLog;

import ai.maum.eng.external.rest.clova.common.ArgumentCheck;
import ai.maum.eng.external.rest.clova.common.PayloadCheck;
import ai.maum.eng.external.rest.clova.common.ResultCode;
import ai.maum.eng.external.rest.clova.common.config.PropertyManager;
import ai.maum.eng.external.rest.clova.grpc.MapGrpcInterfaceManager;
import ai.maum.eng.external.rest.clova.tlo.TLOLogUtills;
import ai.maum.eng.external.rest.clova.tlo.TloInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.DeviceOuterClass.Device.Capability;
import maum.m2u.common.Dialog.Utter;
import maum.m2u.common.Dialog.Utter.InputType;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.EventStream.EventContext;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.supervisor.Monitor.Result;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("clova")
public class ClovaController {

  static final org.slf4j.Logger logger = LoggerFactory
      .getLogger(ClovaController.class);

  private static final String ERROR_MESSAGE = "{\"version\":\"0.1.0\",\"sessionAttributes\":{},\"response\":{\"outputSpeech\":{\"type\":\"SpeechList\",\"values\":[{\"type\":\"PlainText\",\"lang\":\"ko\",\"value\":\"말씀하신 내용을 이해하지 못했어요. 다시 한 번 말씀해주세요.\"}]},\"card\":{},\"directives\":[],\"shouldEndSession\":true}}";
  private static final String GUIDE_MESSAGE = "{\"version\":\"0.1.0\",\"sessionAttributes\":{},\"response\":{\"outputSpeech\":{\"type\":\"SpeechList\",\"values\":[{\"type\":\"PlainText\",\"lang\":\"ko\",\"value\":\"현재 지원되지 않는 기능입니다.\"}]},\"card\":{},\"directives\":[],\"shouldEndSession\":true}}";
  private static final String RESPONSE_MESSAGE = "{\"version\":\"0.1.0\",\"sessionAttributes\":{},\"response\":{\"outputSpeech\":{\"type\":\"SpeechList\",\"values\":[{\"type\":\"PlainText\",\"lang\":\"ko\",\"value\":\"현재 서비스가 원활하지 않습니다. 다시 한번 말씀 해주세요.\"}]},\"card\":{},\"directives\":[],\"shouldEndSession\":true}}";
  private static final String WORK_MESSAGE = "{\"version\":\"0.1.0\",\"sessionAttributes\":{},\"response\":{\"outputSpeech\":{\"type\":\"SpeechList\",\"values\":[{\"type\":\"PlainText\",\"lang\":\"ko\",\"value\":\"유플러스 우리집AI가 업데이트 중이에요 .\"}]},\"card\":{},\"directives\":[],\"shouldEndSession\":true}}";

  private MapGrpcInterfaceManager mapGrpcInterfaceManager;
  private Timestamp.Builder timestamp = Timestamp.newBuilder()
      .setSeconds(System.currentTimeMillis() / 1000);

  private TLOLogUtills tloLogUtills = new TLOLogUtills();
  private Random random = new Random();

  public ClovaController() {
    String[] addrArr = PropertyManager.getString("map.export").split(":");
    this.mapGrpcInterfaceManager = new MapGrpcInterfaceManager(addrArr[0],
        Integer.parseInt(addrArr[1]));
  }

  @RequestMapping(
      value = "",
      method = RequestMethod.POST,
      produces = "application/json")
  @SuppressWarnings("unchecked")
  public ResponseEntity<?> clova(
      @RequestHeader HttpHeaders headers,
      @RequestBody HashMap<String, Object> req) {

    String workNotice = PropertyManager.getString("workNotice");

    if (workNotice.equals("on")) {
      String check = WORK_MESSAGE;
      logger.info("유플러스 우리집AI가 업데이트 중이에요");
      return new ResponseEntity<>(check, HttpStatus.OK);

    }

    logger.debug("======== clova request ========");

    logger.debug(headers.toString());
    logger.debug(req.toString());
    String clovaReqId = "";
    if (headers.containsKey("X-Clova-Dialog-Request-Id")) {
      clovaReqId = headers.get("X-Clova-Dialog-Request-Id").get(0);
    } else {
      clovaReqId = "";
    }

    // Always need Deivce to TLO:sId
    String authDeviceId = "";
    HashMap<String, Object> context = (HashMap) req.get("context");
    if (context != null) {
      HashMap<String, Object> system = (HashMap) context.get("System");
      if (system != null) {
        HashMap<String, Object> device = (HashMap) system.get("device");
        if (device != null) {
          authDeviceId = (String) device.get("deviceId");

        }
      }
    }

    ArgumentCheck argumentCheck = new ArgumentCheck();
    boolean valid = argumentCheck.checkValid(req);
    if (!valid) {
      //Request is not valid

      String curTime1 = getTimeToString1();
      String authTransactionId = "";
      String authMessageId = "";
      String intentType = "";
      String intentValue = "";
      String qValue = "";
      String svcCode = "";
      String resultCode = ResultCode.REQUEST_STRANGE.getCode() + "";

      writeTLO(authDeviceId, curTime1,
          authTransactionId, authMessageId,
          intentType, intentValue, clovaReqId,
          qValue, svcCode, resultCode);
      String result = ERROR_MESSAGE;
      return new ResponseEntity<>(result, HttpStatus.OK);
    }

    try {

      HashMap<String, Object> request = (HashMap) req.get("request");
      HashMap<String, Object> intent = (HashMap) request.get("intent");
      String intentValue = (String) intent.get("intent");
      String result = "Request success";

      // 스피커 발화시 "YBM 영어 말히기 시작해줘","YBM 영어 말하기 X강 시작해줘
      if (intentValue.equals("playYBMtalk") ||
          intentValue.equals("") ||
          intentValue.equals("changeYBMtalk")) {
        logger.debug("request:LaunchRequest, Map:open");
        return open(req, clovaReqId);
        // 전 후 강의 이동, 강의 중지, 영어 STT 실행시(시나리오대로 영어 학습)
      } else if (intentValue.equals("Uplus.MindslabIntent") || intentValue
          .equals("stopYBMtalk")) {
        logger.debug("request:IntentRequest, Map:textToTextTalk");
        return textToTextTalk(req, clovaReqId);
      } else if (intentValue.equals("Clova.GuideIntent")) {

        logger.debug("Service is not support");
        String curTime1 = getTimeToString1();
        String authTransactionId = "";
        String authMessageId = "";
        String intentType = "";
        String qValue = "";
        String svcCode = "";
        String resultCode = ResultCode.REQUEST_STRANGE.getCode() + "";

        writeTLO(authDeviceId, curTime1,
            authTransactionId, authMessageId,
            intentType, intentValue, clovaReqId,
            qValue, svcCode, resultCode);

        result = GUIDE_MESSAGE;
        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        logger.debug("Clova has no request");
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);

      return new ResponseEntity<>(null, HttpStatus.OK);
    }


  }

  @SuppressWarnings("unchecked")
  public ResponseEntity<?> open(HashMap<String, Object> req, String clovaReqId) {

    try {

      HashMap<String, Object> context = (HashMap) req.get("context");

      HashMap<String, Object> system = (HashMap) context.get("System");

      HashMap<String, Object> apllication = (HashMap) system.get("application");
      String authExtentionId = (String) apllication.get("applicationId");

      HashMap<String, Object> device = (HashMap) system.get("device");
      String authDeviceId = (String) device.get("deviceId");

      HashMap<String, Object> user = (HashMap) system.get("user");
      String authAccessToken = (String) user.get("accessToken");

      HashMap<String, Object> requestInfo = (HashMap) req.get("request");
      String intentType = (String) requestInfo.get("type");
      HashMap<String, Object> intent = (HashMap) requestInfo.get("intent");

      String intentValue = (String) intent.get("intent");

      Object request = req.get("request");
      String json = new ObjectMapper().writeValueAsString(request);
      //json to proto message
      Struct.Builder stb = Struct.newBuilder();
      JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

      String curTime1 = getTimeToString1();
      String curTime2 = getTimeToString2();

      String authTransactionId = "TR_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;
      String authMessageId = "MSG_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;

      if (authAccessToken == null) {
        authAccessToken = "";
      }
      String authToken = setAuthInfo(authDeviceId,
          authAccessToken,
          authExtentionId,
          authTransactionId,
          authMessageId,
          clovaReqId);
      logger.info("authToken=" + authToken);

      MapEvent.Builder openParam = makeOpenBuilder(authDeviceId, json);

      logger.info("#@ MAP Open Call = " + authAccessToken + ", " + authDeviceId);
      MapDirective mapResult = mapGrpcInterfaceManager
          .callEvent(openParam.build(), authToken);

      //Map call error
      if (mapResult == null) {
        String svcCode = "OPEN";
        String qValue = "";
        String resultCode = ResultCode.MAP_CALL_ERROR.getCode() + "";

        writeTLO(authDeviceId, curTime1,
            authTransactionId, authMessageId,
            intentType, intentValue, clovaReqId,
            qValue, svcCode, resultCode);
      }

      logger.debug("#@ mapResult = " + mapResult.toString());

      String result = JsonFormat.printer()
          .print(mapResult.getDirective().getPayload());
      logger.debug(" mapResult payload = " + result);

      String svcCode = "OPEN";
      String qValue = "";
//      String resultCode = ResultCode.SUCCESS.getCode() + "";
      String resultCode;
      com.google.protobuf.Struct payload = mapResult.getDirective()
          .getPayload();

      //Success
      if (payload.containsFields("response")) {
        //Da return Sds, Eval, Lms, Db server error check

        Value metaResponse = payload.getFieldsOrThrow("response")
            .getStructValue().getFieldsOrThrow("meta")
            .getStructValue().getFieldsOrThrow("response");

        String payResult = PayloadCheck.getResultCode(metaResponse)
            .replaceAll("\"", "");

        resultCode = payResult;

        logger.debug("#@ resultCode:", resultCode);

        if (resultCode.equals("20000000")) {
          Value resOutputSpeech = metaResponse.getStructValue()
              .getFieldsOrThrow("resOutputSpeech");
          result = JsonFormat.printer().print(resOutputSpeech);

          writeTLO(authDeviceId, curTime1,
              authTransactionId, authMessageId,
              intentType, intentValue, clovaReqId,
              qValue, svcCode, resultCode);

        } else if (resultCode.equals("42000007")) {

          if(metaResponse.getStructValue().containsFields("resOutputSpeech")){
            Value lmsValue = metaResponse.getStructValue()
                .getFieldsOrThrow("resOutputSpeech");
            result = JsonFormat.printer().print(lmsValue);

            writeTLO(authDeviceId, curTime1,
                authTransactionId, authMessageId,
                intentType, intentValue, clovaReqId,
                qValue, svcCode, resultCode);
          }

        } else {
          writeTLO(authDeviceId, curTime1,
              authTransactionId, authMessageId,
              intentType, intentValue, clovaReqId,
              qValue, svcCode, resultCode);

          result = RESPONSE_MESSAGE;
          return new ResponseEntity<>(result, HttpStatus.OK);

        }


      } else {
        //Map return error
        logger.error("I can't found reponse in payload");
        resultCode = ResultCode.MAP_RETURN_NULL.getCode() + "";
        svcCode = "OPEN";
        qValue = "";

        writeTLO(authDeviceId, curTime1,
            authTransactionId, authMessageId,
            intentType, intentValue, clovaReqId,
            qValue, svcCode, resultCode);

        result = RESPONSE_MESSAGE;
        return new ResponseEntity<>(result, HttpStatus.OK);

      }

      return new ResponseEntity<>(result, HttpStatus.OK);

    } catch (Exception e) {
      //Map call error
      e.printStackTrace();
      logger.error("{} => ", e.getMessage(), e);

      String result = RESPONSE_MESSAGE;
      return new ResponseEntity<>(result, HttpStatus.OK);
    }
  }

  @SuppressWarnings("unchecked")
  public ResponseEntity<?> textToTextTalk(HashMap<String, Object> req, String clovaReqId) {

    try {

      HashMap<String, Object> requestValue = (HashMap) req.get("request");
      HashMap<String, Object> intent = (HashMap) requestValue.get("intent");
      HashMap<String, Object> slots = (HashMap) intent.get("slots");
      HashMap<String, Object> q = (HashMap) slots.get("q");

      String qValue = "case";

      boolean isStop = intent.containsValue("stopYBMtalk");
      boolean isGuide = intent.containsValue("Clova.GuideIntent");

      HashMap<String, Object> context = (HashMap) req.get("context");

      HashMap<String, Object> system = (HashMap) context.get("System");

      HashMap<String, Object> apllication = (HashMap) system.get("application");
      String authExtentionId = (String) apllication.get("applicationId");

      HashMap<String, Object> device = (HashMap) system.get("device");
      String authDeviceId = (String) device.get("deviceId");

      HashMap<String, Object> user = (HashMap) system.get("user");
      String authAccessToken = (String) user.get("accessToken");

      // Map 으로 부터 응답 받을 값에서 Clova 에 필요한 형식
      Value metaResponse;
      // Map 으로 부터 응답 받을 값에서 Clova 에 Session 종료 요청 여부
      Value shouldEndSession;
      // Map 으로 부터 응답 받은 값
      String result;

      // 스피커에 Clova로 강제로 호출하여 강제로 종료 한 경우
      if (isStop || isGuide) {
        qValue = "_stop_";
      }

      //q 메시지가 들어 오는 경우
      if (q != null) {
        qValue = (String) q.get("value");
      }

      String curTime1 = getTimeToString1();
      String curTime2 = getTimeToString2();
      String authTransactionId = "TR_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;
      String authMessageId = "MSG_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;
      if (authAccessToken == null) {
        authAccessToken = "";
      }

      String authToken = setAuthInfo(authDeviceId,
          authAccessToken,
          authExtentionId,
          authTransactionId,
          authMessageId,
          clovaReqId);
      logger.debug("authToken=" + authToken);

      HashMap<String, Object> requestInfo = (HashMap) req.get("request");
      String intentType = (String) requestInfo.get("type");
      String intentValue = (String) intent.get("intent");

      MapEvent.Builder talkParam = makeTalkBuilder(authDeviceId, qValue);

      logger.info("#@ MAP Talk Call = " + authAccessToken + ", " + authDeviceId);
      MapDirective mapResult = mapGrpcInterfaceManager
          .callEvent(talkParam.build(), authToken);

      //Map call error
      if (mapResult == null) {
        String svcCode = "TALK";
        String resultCode = ResultCode.MAP_CALL_ERROR.getCode() + "";

        writeTLO(authDeviceId, curTime1,
            authTransactionId, authMessageId,
            intentType, intentValue, clovaReqId,
            qValue, svcCode, resultCode);
        //go exeception
      }

      logger.debug("#@ mapResult = " + mapResult.toString());

      result = JsonFormat.printer()
          .print(mapResult.getDirective().getPayload());
      logger.debug("mapResult = " + result);

      String svcCode = "TALK";
      String resultCode;

      com.google.protobuf.Struct payload = mapResult.getDirective()
          .getPayload();


      //Success
      if (payload.containsFields("response")) {
        //Da return Sds, Eval, Lms, Db server error check

        metaResponse = payload.getFieldsOrThrow("response")
            .getStructValue().getFieldsOrThrow("meta")
            .getStructValue().getFieldsOrThrow("response");

        String payResult = PayloadCheck.getResultCode(metaResponse)
            .replaceAll("\"", "");

        resultCode = payResult;
        logger.info(resultCode);

        if (resultCode.equals("20000000")) {
          Value resOutputSpeech = metaResponse.getStructValue()
              .getFieldsOrThrow("resOutputSpeech");
          result = JsonFormat.printer().print(resOutputSpeech);

          writeTLO(authDeviceId, curTime1,
              authTransactionId, authMessageId,
              intentType, intentValue, clovaReqId,
              qValue, svcCode, resultCode);

        } else if (resultCode.equals("42000007")) {

          if(metaResponse.getStructValue().containsFields("resOutputSpeech")){
            Value lmsValue = metaResponse.getStructValue()
                .getFieldsOrThrow("resOutputSpeech");
            result = JsonFormat.printer().print(lmsValue);

            writeTLO(authDeviceId, curTime1,
                authTransactionId, authMessageId,
                intentType, intentValue, clovaReqId,
                qValue, svcCode, resultCode);
          }

        } else {
          writeTLO(authDeviceId, curTime1,
              authTransactionId, authMessageId,
              intentType, intentValue, clovaReqId,
              qValue, svcCode, resultCode);

          result = RESPONSE_MESSAGE;
          return new ResponseEntity<>(result, HttpStatus.OK);

        }

      } else {

        // Map return error
        logger.error("I can't found reponse in payload ");
        resultCode = ResultCode.MAP_RETURN_NULL.getCode() + "";
        svcCode = "TALK";

        writeTLO(authDeviceId, curTime1,
            authTransactionId, authMessageId,
            intentType, intentValue, clovaReqId,
            qValue, svcCode, resultCode);

        result = RESPONSE_MESSAGE;
        return new ResponseEntity<>(result, HttpStatus.OK);
      }

      shouldEndSession = metaResponse
          .getStructValue().getFieldsOrThrow("resOutputSpeech")
          .getStructValue().getFieldsOrThrow("response")
          .getStructValue().getFieldsOrThrow("shouldEndSession");
      if (shouldEndSession.getBoolValue()) {
        closeMap(req, clovaReqId);
        logger.info("************Map is Closed************");
      }

      return new ResponseEntity<>(result, HttpStatus.OK);

    } catch (Exception e) {
      e.printStackTrace();
      //Map call error
      e.printStackTrace();
      logger.error("{} => ", e.getMessage(), e);

      logger.error("Map server error occured");

      String result = RESPONSE_MESSAGE;
      return new ResponseEntity<>(result, HttpStatus.OK);
    }
  }

  @SuppressWarnings("unchecked")
  public boolean closeMap(HashMap<String, Object> req, String clovaReqId) {

    String authDeviceId;
    String authAccessToken;
    String authExtentionId;

    try {
      HashMap<String, Object> context = (HashMap) req.get("context");

      HashMap<String, Object> system = (HashMap) context.get("System");

      HashMap<String, Object> apllication = (HashMap) system.get("application");
      authExtentionId = (String) apllication.get("applicationId");

      HashMap<String, Object> device = (HashMap) system.get("device");
      authDeviceId = (String) device.get("deviceId");

      HashMap<String, Object> user = (HashMap) system.get("user");
      authAccessToken = (String) user.get("accessToken");
      String curTime1 = getTimeToString1();
      String curTime2 = getTimeToString2();
      String authTransactionId = "TR_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;
      String authMessageId = "MSG_" + curTime2 + "_" + "SVC_001" + "_" + authDeviceId;

      if (authAccessToken == null) {
        authAccessToken = "";
      }

      String authToken = setAuthInfo(authDeviceId,
          authAccessToken,
          authExtentionId,
          authTransactionId,
          authMessageId,
          clovaReqId);
      logger.debug("authToken=" + authToken);

      HashMap<String, Object> requestValue = (HashMap) req.get("request");
      HashMap<String, Object> intent = (HashMap) requestValue.get("intent");

      HashMap<String, Object> requestInfo = (HashMap) req.get("request");

      HashMap<String, Object> slots = (HashMap) intent.get("slots");
      String qValue = "";

      if (slots != null) {
        HashMap<String, Object> q = (HashMap) slots.get("q");
        if (q != null) {
          qValue = (String) q.get("value");
        }
      }

      String intentType = (String) requestInfo.get("type");
      String intentValue = (String) intent.get("intent");
      String svcCode = "CLOSE";
      String resultCode = ResultCode.SUCCESS.getCode() + "";

      MapEvent.Builder closeParam = makeCloseBuilder(authDeviceId);

      logger.info("#@ MAP Close Call = " + authAccessToken + ", " + authDeviceId);
      MapDirective mapResult = mapGrpcInterfaceManager
          .callEvent(closeParam.build(), authToken);

      writeTLO(authDeviceId, curTime1,
          authTransactionId, authMessageId,
          intentType, intentValue, clovaReqId,
          qValue, svcCode, resultCode);

      return true;

    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return false;
    }
  }

  @SuppressWarnings("unchecked")
  public ResponseEntity<?> close(HashMap<String, Object> req) {

    boolean result = closeMap(req, null);
    if (result == true) {
      return new ResponseEntity<>(result, HttpStatus.OK);
    }
    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  private MapEvent.Builder makeOpenBuilder(String authDeviceId, String json) {

    String deviceId = authDeviceId;
    String deviceType = "web";
    String deviceChannel = "1";
    String deviceVersion = "1.0";
    String location = "korean";
    String longitude = "3.4567678";
    String latitude = "2.312312";

    MapEvent.Builder openParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("DialogWeb")
                .setOperation("Open")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID()).addContexts(
                EventContext.newBuilder().setDevice(
                    Device.newBuilder().setId(deviceId)
                        .setType(deviceType)
                        .setVersion(deviceVersion)
                        .setChannel(deviceChannel)
                        .setSupport(
                            Capability.newBuilder().setSupportRenderText(true)
                                .setSupportRenderCard(true)
                                .setSupportSpeechSynthesizer(false)
                                .setSupportPlayAudio(false)
                                .setSupportPlayVideo(true)
                                .setSupportAction(false)
                                .setSupportMove(false)
                                .setSupportExpectSpeech(true))
                        .setTimestamp(timestamp).setTimezone("KST+9")))
            .addContexts(
                EventContext.newBuilder().setLocation(
                    Location.newBuilder()
                        .setLatitude(Float.parseFloat(latitude))
                        .setLongitude(Float.parseFloat(longitude))
                        .setLocation(location)
                        .build()))
            .setPayload(Struct.newBuilder()
                .putFields("utter",
                    Value.newBuilder().setStringValue("").build())
                .putFields("chatbot",
                    Value.newBuilder().setStringValue("CHATBOT1").build())
                .putFields("skill",
                    Value.newBuilder().setStringValue("english").build())
                .putFields("lang",
                    Value.newBuilder().setStringValue("en_US").build())
                .putFields("meta",
                    Value.newBuilder().setStructValue(jsonToStruct(json)).build())
            ));
    return openParam;
  }

  private MapEvent.Builder makeTalkBuilder(String authDeviceId, String utter)
      throws Exception {

    String deviceId = authDeviceId;
    String deviceType = "web";
    String deviceChannel = "1";
    String deviceVersion = "1.0";
    String location = "korean";
    String longitude = "3.4567678";
    String latitude = "2.312312";
    String payload_utter = utter;

    Utter.Builder talkPayload = Utter.newBuilder()
        .setUtter(payload_utter)
        .setInputType(InputType.valueOf("KEYBOARD"))
        .setLang(Lang.en_US);
    String json = JsonFormat.printer().includingDefaultValueFields()
        .print(talkPayload);
    Struct.Builder stb = Struct.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder talkParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("DialogWeb")
                .setOperation("TextToTextTalk")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID()).addContexts(
                EventContext.newBuilder().setDevice(
                    Device.newBuilder().setId(deviceId)
                        .setType(deviceType)
                        .setVersion(deviceVersion)
                        .setChannel(deviceChannel)
                        .setSupport(
                            Capability.newBuilder().setSupportRenderText(true)
                                .setSupportRenderCard(true)
                                .setSupportSpeechSynthesizer(false)
                                .setSupportPlayAudio(false)
                                .setSupportPlayVideo(true)
                                .setSupportAction(false)
                                .setSupportMove(false)
                                .setSupportExpectSpeech(true))
                        .setTimestamp(timestamp).setTimezone("KST+9")))
            .addContexts(
                EventContext.newBuilder().setLocation(
                    Location.newBuilder()
                        .setLatitude(Float.parseFloat(latitude))
                        .setLongitude(Float.parseFloat(longitude))
                        .setLocation(location)
                        .build()))
            .setPayload(stb));
    return talkParam;
  }

  private MapEvent.Builder makeCloseBuilder(String authDeviceId) {

    String deviceId = authDeviceId;
    String deviceType = "web";
    String deviceChannel = "1";
    String deviceVersion = "1.0";
    String location = "korean";
    String longitude = "3.4567678";
    String latitude = "2.312312";

    MapEvent.Builder closeParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("DialogWeb")
                .setOperation("Close")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID()).addContexts(
                EventContext.newBuilder().setDevice(
                    Device.newBuilder().setId(deviceId)
                        .setType(deviceType)
                        .setVersion(deviceVersion)
                        .setChannel(deviceChannel)
                        .setSupport(
                            Capability.newBuilder().setSupportRenderText(true)
                                .setSupportRenderCard(true)
                                .setSupportSpeechSynthesizer(false)
                                .setSupportPlayAudio(false)
                                .setSupportPlayVideo(true)
                                .setSupportAction(false)
                                .setSupportMove(false)
                                .setSupportExpectSpeech(true))
                        .setTimestamp(timestamp).setTimezone("KST+9")))
            .addContexts(
                EventContext.newBuilder().setLocation(
                    Location.newBuilder()
                        .setLatitude(Float.parseFloat(latitude))
                        .setLongitude(Float.parseFloat(longitude))
                        .setLocation(location)
                        .build()))
            .setPayload(Struct.newBuilder()));
    return closeParam;
  }


  private static String getUUID() {
    return UUID.randomUUID().toString();
  }

  public String getTimeToString1() {
    DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    Calendar calendar = Calendar.getInstance();
    String strDate = df.format(calendar.getTime());
    return strDate;
  }

  public String getTimeToString2() {
    DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    Calendar calendar = Calendar.getInstance();
    String strDate = df.format(calendar.getTime());
    return strDate;
  }

  public String setAuthInfo(String authDeviceId,
      String authAccessToken,
      String authExtentionId,
      String authTransactionId,
      String authMessageId,
      String clovaReqId) {
    String authToken = "{";
    authToken += String.format(" \"deviceId\":\"%s\",", authDeviceId);
    authToken += String.format(" \"accessToken\":\"%s\",", authAccessToken);
    authToken += String.format(" \"extentionId\":\"%s\",", authExtentionId);
    authToken += String.format(" \"transactionId\":\"%s\",", authTransactionId);
    authToken += String.format(" \"messageId\":\"%s\",", authMessageId);
    authToken += String.format(" \"clovaReqId\":\"%s\"", clovaReqId);
    authToken += "}";
    return authToken;
  }

  public Struct jsonToStruct(String msg) {
    Struct.Builder stb = Struct.newBuilder();
    try {
      String json = msg;
      JsonFormat.parser().ignoringUnknownFields().merge(json, stb);
    } catch (InvalidProtocolBufferException e) {
      return stb.build();
    }
    return stb.build();
  }

  private void writeTLO(String authDeviceId, String curTime1,
      String authTransactionId, String authMessageId,
      String intentType, String intentValue, String clovaReqId,
      String qValue, String svcCode, String resultCode) {

    int ranNumber = random.nextInt(9000) + 1000;
    TloInfo info = new TloInfo();
    info.seq_id = curTime1 + ranNumber;
    info.log_type = "SVC";
    info.sid = authDeviceId;
    info.result_code = resultCode;
    info.req_time = curTime1;
    info.client_ip = "";
    info.dev_info = "SPK";
    info.os_info = "";
    info.nw_info = "";
    info.svc_name = "EXTENSION";
    info.dev_model = "";
    info.carrier_type = "L";
    info.tr_id = authTransactionId;
    info.msg_id = authMessageId;
    info.from_svc_name = "CLOVA";
    info.to_svc_name = "MAP";
    info.svc_type = "SVC_001";
    info.dev_type = "DEV_001";
    info.dev_token = "";
    info.intent_type = intentType;
    info.intent_value = intentValue;
    info.stt_text = qValue;
    info.clovareq_id = clovaReqId;
    info.svc_code = svcCode;
    info.rsp_time = getTimeToString1();
    info.log_time = info.rsp_time.substring(0, info.req_time.length() - 3);
    makeTLOLog(info);

  }
}
