package ai.maum.eng.external.rest.clova.tlo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.MarkerFactory;

public class TLOLogUtills {

  static final Logger logger = LoggerFactory.getLogger(TLOLogUtills.class);

  public static void makeTLOLog(TloInfo info) {
    logger.info("#@ START makeTLOLog");

    logger.info(MarkerFactory.getMarker("TLOLog"), "", info);
    MDC.clear();
  }
}
