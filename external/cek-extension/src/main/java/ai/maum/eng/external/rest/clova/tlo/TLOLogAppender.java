package ai.maum.eng.external.rest.clova.tlo;


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;

/**
 * 5분 단위로 파일을 갱신 하면서 기록하는 Appender
 */
public class TLOLogAppender extends RollingFileAppender<ILoggingEvent> {

  private static final String allowedMarker = "TLOLog";

  /**
   * RollingFileAppender rollover()를 수행한다.
   */
  public void performRollover() {
    super.rollover();
  }

  /**
   * 시작할 때, RollingPolicy에 RollingFileAppender를 설정한다.
   */
  @Override
  public void start() {
    ((FiveMinuteRollingPolicy) getRollingPolicy()).setRollingFileAppender(this);
    super.start();
  }

  /**
   * 새로 생성할 로그 파일 이름을 전달한다.
   */
  @Override
  public String getFile() {
    return getRollingPolicy().getActiveFileName();
  }

  /**
   * 기존 rollover()는 무효화 시킨다.
   */
  @Override
  public void rollover() {
    return;
  }

  /**
   * Marker가 매칭되는 로그만 append 한다.
   */
  @Override
  protected void append(ILoggingEvent event) {
    if (!event.getMarker().contains(allowedMarker)) {
      return;
    }

    super.append(event);
  }
}

