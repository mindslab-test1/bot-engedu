syntax = "proto3";

package maum.m2u.server;

import "google/protobuf/empty.proto";
import "google/protobuf/duration.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/struct.proto";
import "maum/common/lang.proto";
import "maum/m2u/common/mediatype.proto";
import "maum/m2u/facade/dialog.proto";
import "maum/m2u/da/provider.proto";
import "maum/m2u/server/cl.proto";
import "maum/m2u/facade/front.proto";

/// ADMIN

message SessionContextQuery {
  string key = 1;
  bool exact_match = 2;
}

message SessionContext {
  map<string, string> properties = 1;
}

service SessionService {
  rpc GetSession (maum.m2u.facade.SessionKey) returns (maum.m2u.facade.Session);
  rpc GetSessionContext (SessionContextQuery) returns (SessionContext);
  rpc GetCaller (maum.m2u.facade.SessionKey) returns (maum.m2u.facade.Caller);
}

/// ADMIN

message TalkAgent {
  string skill = 1;
  string intent = 2;
  string agent = 3;
  maum.m2u.da.AgentKind agent_kind = 4;
  maum.m2u.da.DialogSessionState state = 5;
  int32 turn = 6;
  maum.common.LangCode lang = 7;
}

// 한턴의 대화 전체 기록
message SessionTalk {
  // 순서
  uint32 seq = 1;
  // DialogSession.id
  int32 session_id = 2;
  // 언어
  maum.common.LangCode lang = 11;
  // 음성 여부
  bool audio = 12;
  // Sample Rate
  int32 sample_rate = 13;
  // 이미지 여부
  bool image = 14;
  // 입력 텍스트
  string in = 21;
  string in_mangled = 22;
  // 출력 텍스트
  string out = 23;
  // 출력 음성
  string audio_record_file = 24;
  // 출력 메타 데이터
  map<string, string> meta = 31;
  // 출력 메타 데이터
  map<string, string> context = 32;
  // grpc 최종 status
  int32 status_code = 41;
  // 내부 처리 코드
  maum.m2u.facade.DialogResult dialog_result = 42;
  // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
  // NamedEntityAnalysis named_entity_analysis = 50;
  // 복수의 대화 에이전트 정보
  repeated TalkAgent agents = 61;
  // 최종 도메인
  string skill = 62;
  // 최종 의도
  string intent = 63;
  // 시작 시간
  google.protobuf.Timestamp start = 71;
  // 끝 시간
  google.protobuf.Timestamp end = 72;
  // 각 구간별 시간
  map<string, google.protobuf.Duration> elapsed_time = 73;
  // 각 세부 동작별 CL
  ConfidenceLevel cl = 81;
  float cl_result = 82;
  // CL 안정구간이 아닌 경우 표시
  int32 warned = 83;
  // 사용자 피드백
  int32 feedback = 91;
  // 대화 정확도
  int32 accuracy = 92;

  // 다른 챗봇으로 전달을 위한 챗봇 이름
  string transit_chatbot = 101;
  string transit_from = 102;
}

// agent 정보
message Agent {
  string name = 1;
  maum.common.LangCode lang = 2;
  int32 turn = 3;
  string agent_key = 4;
  string skill = 5;
  string key = 6; // 멀티턴의 경우 키를 이용해서 기존의 DA를 재활용하도록 한다.
}

message AgentUserAttribute {
  string agent_key = 1;
  string skill = 2;
  map<string, string> user_attrs = 3;
}

// 세션 정보
message DialogSession {
  int64 id = 1;
  string chatbot = 2;
  string user_key = 3;
  google.protobuf.Struct contexts = 4;
  google.protobuf.Duration duration = 5;
  bool human_assisted = 6;
  string name = 7;
  Agent current_agent = 8;
  maum.m2u.common.MediaType output = 9;
  string intent_finder = 10;
  bool valid = 15;
  bool active = 16;
  google.protobuf.Timestamp started_at = 20;
  google.protobuf.Timestamp talked_at = 21;
  maum.m2u.facade.Caller caller = 30;
  string peer = 31;
  int32 warned_talk_count = 41;
  ConfidenceLevel cl = 42;
  float last_cl_result = 43;
  bool interfered = 44;
  repeated AgentUserAttribute agent_user_attrs = 50;
  // 디바이스 정보
  string device_token = 51;
  maum.m2u.facade.Device device_info = 52;
  // Peek Answer 상태 flag
  bool is_peek_answered = 61;
  string peek_answer = 62;
  string transit_chatbot = 63;
}

// 세션 요약 정보
message SessionSummary {
  int64 session_id = 1;
  maum.m2u.facade.Session session = 2;
  int32 talks_count = 3;
  int32 warned_talk_count = 4;
  ConfidenceLevel cl = 5;
  float last_cl_result = 6;
}

// 상세 세션 정보
message SessionDetail {
  // 세션 id
  int64 session_id = 1;
  // 세션
  maum.m2u.facade.Session session = 2;
  // 대화 목록
  repeated SessionTalk talks = 3;
  int32 warned_talk_count = 4;
  ConfidenceLevel cl = 5;
  // Customer 이름
  string customer = 6;
}

// 세션 요약 정보 목록
message SessionList {
  repeated SessionSummary sessions = 1;
}

// 세션 응답 상태
message SessionResponseStatus {
  int64 session_id = 1;
  bool human_assisted = 2;
}

message SessionQuery {
  // 전체 조회
  bool all = 1;
  // 챗봇
  string chatbot = 2;
  // AccessFrom
  maum.m2u.facade.AccessFrom access_from = 3;
  float cl_upper_bound = 4;
  float cl_lower_bound = 5;
  // 속성
  map<string, string> properties = 6;
  // 대화 횟수
  int64 talk_cnt = 7;
  // 스킬
  string skill = 8;
  // Customer 이름
  string customer = 9;
}

message SessionDetailList {
  repeated SessionDetail sessions = 1;
}

// Confidence Level을 구하는 작업과 관련된 작업들
// 아래의 모든 작업은 auth.key meta를 항상 요구한다.
service SessionAdmin {
  rpc GetSessionDetails (SessionQuery) returns (SessionDetailList);
  rpc GetSessions (SessionQuery) returns (SessionList);
  rpc GetSession (maum.m2u.facade.SessionKey) returns (SessionDetail);
  rpc JoinSession (maum.m2u.facade.SessionKey) returns (SessionResponseStatus);
  rpc RestoreSession (maum.m2u.facade.SessionKey) returns (SessionResponseStatus);
  rpc TerminateSession (maum.m2u.facade.SessionKey) returns (maum.m2u.facade.SessionKey);
}

// 신뢰레벨 파라미터
message ConfidenceLevelParam {
  map<int32, float> elem_ratios = 1;
  map<int32, float> elem_threshold = 2;
  float lower_bound = 3;
  float upper_bound = 4;
}

message ChatbotConfidenceLevelParam {
  map<string, ConfidenceLevelParam> svc_grp_params = 1;
}

service ConfidenceLevelAdmin {
  rpc SetConfidenceLevelParam (ChatbotConfidenceLevelParam) returns (google.protobuf.Empty);
}
