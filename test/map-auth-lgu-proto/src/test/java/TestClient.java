import org.junit.Test;

import static junit.framework.TestCase.fail;

/**
 * Created by jaeheoncho on 2018. 5. 17..
 */

public class TestClient {


    @Test
    public void testClient(){
        System.out.println("---------client test--------");

        ai.maum.m2u.test.TestClient client = new ai.maum.m2u.test.TestClient();
        client.testMethod();
        try {
            client.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
