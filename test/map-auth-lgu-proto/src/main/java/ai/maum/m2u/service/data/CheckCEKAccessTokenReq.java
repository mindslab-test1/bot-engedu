package ai.maum.m2u.service.data;

import java.io.Serializable;

/**
 * Created by jaeheoncho on 2018. 5. 17..
 */
public class CheckCEKAccessTokenReq implements Serializable{

    private ReqHeader header;
    private Params params;

    public CheckCEKAccessTokenReq() {
    }

    public ReqHeader getHeader() {
        return header;
    }

    public void setHeader(ReqHeader header) {
        this.header = header;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public static class ReqHeader implements Serializable{
        private String svrKey;
        private String transactionid;
        private String messageid;
        private String customId;

        public ReqHeader() {
        }

        public String getSvrKey() {
            return svrKey;
        }

        public void setSvrKey(String svrKey) {
            this.svrKey = svrKey;
        }

        public String getTransactionid() {
            return transactionid;
        }

        public void setTransactionid(String transactionid) {
            this.transactionid = transactionid;
        }

        public String getMessageid() {
            return messageid;
        }

        public void setMessageid(String messageid) {
            this.messageid = messageid;
        }

        public String getCustomId() {
            return customId;
        }

        public void setCustomId(String customId) {
            this.customId = customId;
        }
    }

    public static class Params implements Serializable{

        private Common common;
        private Body body;

        public Params() {
        }

        public Common getCommon() {
            return common;
        }

        public void setCommon(Common common) {
            this.common = common;
        }

        public Body getBody() {
            return body;
        }

        public void setBody(Body body) {
            this.body = body;
        }
    }

    public static class Common{

        private String  clientIp;
        private String  devInfo;
        private String  osInfo;
        private String  nwInfo;
        private String  devModel;
        private String  carrierType;

        public Common() {
        }

        public String getClientIp() {
            return clientIp;
        }

        public void setClientIp(String clientIp) {
            this.clientIp = clientIp;
        }

        public String getDevInfo() {
            return devInfo;
        }

        public void setDevInfo(String devInfo) {
            this.devInfo = devInfo;
        }

        public String getOsInfo() {
            return osInfo;
        }

        public void setOsInfo(String osInfo) {
            this.osInfo = osInfo;
        }

        public String getNwInfo() {
            return nwInfo;
        }

        public void setNwInfo(String nwInfo) {
            this.nwInfo = nwInfo;
        }

        public String getDevModel() {
            return devModel;
        }

        public void setDevModel(String devModel) {
            this.devModel = devModel;
        }

        public String getCarrierType() {
            return carrierType;
        }

        public void setCarrierType(String carrierType) {
            this.carrierType = carrierType;
        }
    }

    public static class Body {
        private String  serviceType;
        private Cek cek;

        public Body(){}

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public Cek getCek() {
            return cek;
        }

        public void setCek(Cek cek) {
            this.cek = cek;
        }
    }

    public static class Cek{
        private String deviceId;
        private String accessToken;
        private String extentionId;

        public Cek(){}

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getExtentionId() {
            return extentionId;
        }

        public void setExtentionId(String extentionId) {
            this.extentionId = extentionId;
        }
    }
}
