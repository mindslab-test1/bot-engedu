package ai.maum.m2u.service.data;

import java.io.Serializable;

/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class GetCEKAccessTokenCheckResp implements Serializable{

    private String access_token;
    private String refresh_token;
    private String expire_in;
    private String token_type;
    private String redirect_uri;
    private String client_id;
    private String scope;

    public GetCEKAccessTokenCheckResp() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getExpire_in() {
        return expire_in;
    }

    public void setExpire_in(String expire_in) {
        this.expire_in = expire_in;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRedirect_uri() {
        return redirect_uri;
    }

    public void setRedirect_uri(String redirect_uri) {
        this.redirect_uri = redirect_uri;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "GetCEKAccessTokenCheckResp{" +
                "access_token='" + access_token + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", expire_in='" + expire_in + '\'' +
                ", token_type='" + token_type + '\'' +
                ", redirect_uri='" + redirect_uri + '\'' +
                ", client_id='" + client_id + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }
}
