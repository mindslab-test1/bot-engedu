package ai.maum.m2u.service.data;

import java.io.Serializable;

/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class GetAuthorizationCodeResp implements Serializable {

    private Common common;
    private Body body;

    public GetAuthorizationCodeResp(){}

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }



    public static class Common {
        private String resultCode;
        private String resultMessage;

        public Common(){}

        public String getResultCode() {
            return resultCode;
        }

        public void setResultCode(String resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }
    }

    public static class Body{

        private Cek cek;

        public Body(){}

        public Cek getCek() {
            return cek;
        }

        public void setCek(Cek cek) {
            this.cek = cek;
        }
    }

    public static class Cek{

        private String authorizationCd;

        public Cek(){}

        public String getAuthorizationCd() {
            return authorizationCd;
        }

        public void setAuthorizationCd(String authorizationCd) {
            this.authorizationCd = authorizationCd;
        }
    }
}
