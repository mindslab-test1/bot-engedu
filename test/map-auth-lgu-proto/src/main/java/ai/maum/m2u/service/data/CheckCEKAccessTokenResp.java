package ai.maum.m2u.service.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class CheckCEKAccessTokenResp implements Serializable{

    private Common common;
    private Body body;

    public CheckCEKAccessTokenResp() {
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }



    public static class Common implements Serializable{

        private String resultCode;
        private String resultMessage;

        public Common() {
        }

        public String getResultCode() {
            return resultCode;
        }

        public void setResultCode(String resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }
    }

    public static class Body implements Serializable {

        private String deviceToken;
        private String deviceId;
        private String customId;
        private String oneId;
        private String nid;
        private String svcType;
        private String devType;

        private List<AuthInfo> authInfos;

        public Body() {
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getCustomId() {
            return customId;
        }

        public void setCustomId(String customId) {
            this.customId = customId;
        }

        public String getOneId() {
            return oneId;
        }

        public void setOneId(String oneId) {
            this.oneId = oneId;
        }

        public String getNid() {
            return nid;
        }

        public void setNid(String nid) {
            this.nid = nid;
        }

        public String getSvcType() {
            return svcType;
        }

        public void setSvcType(String svcType) {
            this.svcType = svcType;
        }

        public String getDevType() {
            return devType;
        }

        public void setDevType(String devType) {
            this.devType = devType;
        }

        public List<AuthInfo> getAuthInfos() {
            if(authInfos == null){
                authInfos = new ArrayList<>();
            }
            return authInfos;
        }

        public void setAuthInfos(List<AuthInfo> authInfos) {
            this.authInfos = authInfos;
        }
    }

    public static class AuthInfo implements Serializable{

        private String contSvcCd;
        private String logoutYn;
        private String userContUseYn;
        private String svcUseYn;
        private String svcContGrantUseYn;
        private String svcContUseYn;
        private String svcType;
        private String devType;
        private String authParameter1;
        private String authParameter2;
        private String authParameter3;
        private String authParameter4;
        private String authParameter5;
        private String authParameter6;
        private String authParameter7;
        private String authParameter8;
        private String authParameter9;
        private String authParameter10;

        public AuthInfo() {
        }

        public String getContSvcCd() {
            return contSvcCd;
        }

        public void setContSvcCd(String contSvcCd) {
            this.contSvcCd = contSvcCd;
        }

        public String getLogoutYn() {
            return logoutYn;
        }

        public void setLogoutYn(String logoutYn) {
            this.logoutYn = logoutYn;
        }

        public String getUserContUseYn() {
            return userContUseYn;
        }

        public void setUserContUseYn(String userContUseYn) {
            this.userContUseYn = userContUseYn;
        }

        public String getSvcUseYn() {
            return svcUseYn;
        }

        public void setSvcUseYn(String svcUseYn) {
            this.svcUseYn = svcUseYn;
        }

        public String getSvcContGrantUseYn() {
            return svcContGrantUseYn;
        }

        public void setSvcContGrantUseYn(String svcContGrantUseYn) {
            this.svcContGrantUseYn = svcContGrantUseYn;
        }

        public String getSvcContUseYn() {
            return svcContUseYn;
        }

        public void setSvcContUseYn(String svcContUseYn) {
            this.svcContUseYn = svcContUseYn;
        }

        public String getSvcType() {
            return svcType;
        }

        public void setSvcType(String svcType) {
            this.svcType = svcType;
        }

        public String getDevType() {
            return devType;
        }

        public void setDevType(String devType) {
            this.devType = devType;
        }

        public String getAuthParameter1() {
            return authParameter1;
        }

        public void setAuthParameter1(String authParameter1) {
            this.authParameter1 = authParameter1;
        }

        public String getAuthParameter2() {
            return authParameter2;
        }

        public void setAuthParameter2(String authParameter2) {
            this.authParameter2 = authParameter2;
        }

        public String getAuthParameter3() {
            return authParameter3;
        }

        public void setAuthParameter3(String authParameter3) {
            this.authParameter3 = authParameter3;
        }

        public String getAuthParameter4() {
            return authParameter4;
        }

        public void setAuthParameter4(String authParameter4) {
            this.authParameter4 = authParameter4;
        }

        public String getAuthParameter5() {
            return authParameter5;
        }

        public void setAuthParameter5(String authParameter5) {
            this.authParameter5 = authParameter5;
        }

        public String getAuthParameter6() {
            return authParameter6;
        }

        public void setAuthParameter6(String authParameter6) {
            this.authParameter6 = authParameter6;
        }

        public String getAuthParameter7() {
            return authParameter7;
        }

        public void setAuthParameter7(String authParameter7) {
            this.authParameter7 = authParameter7;
        }

        public String getAuthParameter8() {
            return authParameter8;
        }

        public void setAuthParameter8(String authParameter8) {
            this.authParameter8 = authParameter8;
        }

        public String getAuthParameter9() {
            return authParameter9;
        }

        public void setAuthParameter9(String authParameter9) {
            this.authParameter9 = authParameter9;
        }

        public String getAuthParameter10() {
            return authParameter10;
        }

        public void setAuthParameter10(String authParameter10) {
            this.authParameter10 = authParameter10;
        }
    }
}
