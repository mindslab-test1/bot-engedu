package ai.maum.m2u;

import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.service.AuthenticationImpl;
import ai.maum.m2u.service.AuthorizationImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class MapAuthServer {

  static final Logger logger = LoggerFactory.getLogger(MapAuthServer.class);

  private Server server;
  private PropertyManager config;

  private void start() throws IOException {
    logger.info("map-auth Server starting");

//    int port = this.config.getInt("auth.export.port");
//    int grpcTimeout = this.config.getInt("auth.grpc.timeout");
    int port = 10100;
    int grpcTimeout = 30;

    ServerBuilder sb = NettyServerBuilder.forPort(port)
        .maxConnectionIdle(grpcTimeout, TimeUnit.MILLISECONDS)
        .maxConnectionAge(grpcTimeout, TimeUnit.MILLISECONDS);

    sb.addService(new AuthenticationImpl());
    sb.addService(new AuthorizationImpl());

    // grpc cli instance
    sb.addService(ProtoReflectionService.newInstance());

    server = sb.build().start();

    logger.info("MapAuthServer Server started, listening on " + port);
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        // Use stderr here since the logger may have been reset by its
        // JVM shutdown hook.
        logger.info("*** shutting down gRPC server since JVM is shutting down");
        MapAuthServer.this.stop();
        logger.info("*** server shut down");
      }
    });
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    final MapAuthServer server = new MapAuthServer();
    server.start();
    server.blockUntilShutdown();
  }

}
