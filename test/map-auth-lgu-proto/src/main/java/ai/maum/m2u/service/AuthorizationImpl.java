package ai.maum.m2u.service;

import ai.maum.m2u.test.TestFinalValue;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Authentication.GetUserInfoRequest;
import maum.m2u.map.Authentication.GetUserInfoResponse;
import maum.m2u.map.Authentication.IsValidRequest;
import maum.m2u.map.Authentication.IsValidResponse;
import maum.m2u.map.AuthorizationProviderGrpc.AuthorizationProviderImplBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthorizationImpl extends AuthorizationProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthorizationImpl.class);

  @Override
  public void isValid(IsValidRequest request, StreamObserver<IsValidResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.isValid");

    try {

      IsValidResponse.Builder isValidResponse = IsValidResponse.newBuilder();

      if (TestFinalValue.FIX_AUTH_TOKEN.equals(request.getAuthToken())) {
        isValidResponse.setIsValid(true);
        isValidResponse.setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN);

        Timestamp.Builder timestamp = Timestamp.newBuilder().setSeconds(System.currentTimeMillis() + 100000);
        isValidResponse.setExpiredAt(timestamp);
      } else {
        throw new Exception("Un Valid Auth Token");
      }

      responseObserver.onNext(isValidResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getUserInfo(GetUserInfoRequest request,
      StreamObserver<GetUserInfoResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.getUserInfo");

    try {

      GetUserInfoResponse.Builder getUserInfoResponse = GetUserInfoResponse.newBuilder();

      if (TestFinalValue.FIX_ACCESS_TOKEN.equals(request.getAccessToken())) {
        User.Builder user = User.newBuilder();
        Struct.Builder struct = Struct.newBuilder();
        String json = null;
        json = JsonFormat.printer().includingDefaultValueFields().print(Empty.newBuilder());
        JsonFormat.parser().ignoringUnknownFields().merge(json, struct);

        user.setUserId(TestFinalValue.FIX_USER_KEY);
        user.setName("mindsLab");
        user.setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN);
        user.setMeta(struct);

        getUserInfoResponse.setUser(user);

      } else {
        throw new Exception("Un Valid Access Token");
      }

      responseObserver.onNext(getUserInfoResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
