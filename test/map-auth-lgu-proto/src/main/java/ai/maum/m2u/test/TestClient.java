package ai.maum.m2u.test;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import junit.framework.Test;
import maum.m2u.map.Authentication;
import maum.m2u.map.Authentication.LGUCheckCekTokenReq;
import maum.m2u.map.AuthenticationProviderGrpc;

import java.util.concurrent.TimeUnit;


/**
 * Created by jaeheoncho on 2018. 5. 16..
 */
public class TestClient {

    public AuthenticationProviderGrpc.AuthenticationProviderBlockingStub clinetStub;
    public ManagedChannel channel;

    public TestClient(){

        channel = ManagedChannelBuilder.forAddress("localhost", 10100).usePlaintext(true).build();
        clinetStub = AuthenticationProviderGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException{
        channel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
    }

    public void testMethod() {
        LGUCheckCekTokenReq.Builder req = LGUCheckCekTokenReq.newBuilder();
        Authentication.LGUAuthReqHeader.Builder header = Authentication.LGUAuthReqHeader.newBuilder();
        header.setCustomId("custid xxx");
        header.setMessageid("msgid xxx");
        header.setSvrKey("svrkey xxx");
        header.setTransactionid("transactionid xxx");
        req.setHeader(header.build());

        Authentication.LGUCheckCekTokenReqParam.Builder reqParam = Authentication.LGUCheckCekTokenReqParam.newBuilder();
        Authentication.LGUAuthReqCommon.Builder comm = Authentication.LGUAuthReqCommon.newBuilder();
        comm.setCarrierType("CCC");
        comm.setClientIp("client ip");
        comm.setDevInfo("devInfo");
        comm.setDevModel("dev model");
        comm.setNwInfo("nw info");
        comm.setOsInfo("os info");
        reqParam.setCommon(comm.build());

        Authentication.LGUCheckCekTokenReqBody.Builder body =Authentication.LGUCheckCekTokenReqBody.newBuilder();
        body.setServiceType("SVC_001");
        Authentication.CheckCek.Builder cek = Authentication.CheckCek.newBuilder();
        cek.setExtentionId("extid..");
        cek.setDeviceId("devid..");
        cek.setAccessToken("token....");
        body.setCek(cek.build());
        reqParam.setBody(body.build());


        try {
            Authentication.LGUCheckCekTokenResp resp = this.clinetStub.checkCEKAccessToken(req.build());
            System.out.println("out: \n"+resp);
        }catch(Exception e){
            System.out.println("error:" + e.getMessage());
        }
    }

    public static void main(String[] args){
        System.out.println("gRPC Client TEST");
        TestClient client = new TestClient();
        client.testMethod();
        try {
            client.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
