const express = require('express');
// const jwt = require('jsonwebtoken');
// const mqtt = require('mqtt');
const app = express();
const http = require('http');
require('console-stamp')(console, '[HH:MM:ss.l]');

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());

// var jwt_secret = 'merongmerongmerong~~!!';
var allowCORS = function (req, res, next) {
  // console.log('allowCORS');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    (req.method === 'OPTIONS') ?
        res.sendStatus(200) :
        next();
};
app.use(allowCORS);

//logger
// express.logger.format('myudate', function() {
//     var df = require('console-stamp/node_modules/dateformat');
//     return df(new Date(), 'HH:MM:ss.l');
// });
// app.use(express.logger('[:mydate] :method :url :status :res[content-length] - :remote-addr - :response-time ms'));

function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}

app.use(nocache);

app.post('/auth/checkCEKAccessToken', (req, res) => {
  console.log("/auth/checkCEKAccessToken");

  var out = {
    "common": {
      "resultCode": "20000000",
      "resultMessage": ""
    },
    "body": {
      "deviceToken": "xxxxxxxxxxxxx",
      "deviceId": "xxxxxxxxxxxxx",
      "customId": "xxxxxxxxx",
      "oneId": "xxxxxxxxx",
      "nid": "xxxxxxxxx",
      "svcType": "SVC_001",
      "devType": "DEV_001",
      "authInfos": [
        {
          "contSvcCd": "iot",
          "authParameter1": "TESTID",
          "authParameter2": "TESTVALUE",
          "authParameter3": "",
          "authParameter4": "",
          "authParameter5": "",
          "logoutYn": "N",
          "deviceContSvcUseYn": "Y",
          "svcUseYn": "Y",
          "svcDeviceGrantUseYn": "Y",
          "deviceUseYn": "Y",
          "svcContGrantUseYn": "Y",
          "svcContUseYn": "Y",
          "userContUseYn": "Y",
          "svcType": "SVC_001",
          "devType": "DEV_001"
        }
      ]
    }
  }

  res.send(out);
});



app.listen(3300, () => {
  console.log('Example REST app listening on port 3300!');
});


