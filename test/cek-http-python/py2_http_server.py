#!/usr/bin/env python
"""
example
   ./py2_http_server.py 8000
   
"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import json

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()


    def do_GET(self):
        self._set_headers()
        self.wfile.write("hello world!!!")

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        print '===Request:Start==='
        print self.requestline
        print self.headers

        launch_response = "dummy"
        file = open('launch_response.json')
        launch_response = file.read()

        intent_response = "dummy"
        file = open('intent_response.json')
        intent_response = file.read()

        unknown_response = "{}"

        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        print post_body
        print '===Request:End==='
        self._set_headers()
        response = ""
        if (post_body.find("LaunchRequest") != -1):
            print 'found LaunchRequest'
            self.wfile.write(launch_response)
            response = launch_response;
        elif (post_body.find("IntentRequest") != -1):
            print 'found IntentRequest'
            self.wfile.write(intent_response)
            response = intent_response
        else:
            self.wfile.write(unknown_response)
            response = unknown_response
        print '===Response:Start==='
        print response
        print '===Response:End==='

def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'start httpd...', port
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
