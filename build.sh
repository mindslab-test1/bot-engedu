#!/usr/bin/env bash

function usage() {
  echo "build.sh help : this message"
  echo "buiid.sh MAUM_ROOT: build all targets"
  echo "build.sh tar: archive MAUM_ROOT folder as tar.gz"

  echo "build.sh clean-deploy: clean tar temp directories."
  echo "build.sh clean-cache: clean build cache directories."
  echo "build.sh clean-cmake: clean build cache directories."
}

if [ "$#" -lt 1 ]; then
  echo "Illegal number of parameters"
  echo
  usage
  exit 1
fi

if [ "$1" = "help" ]; then
  usage
  exit 0
fi

if [ "$1" = "clean-deploy" ]; then
  echo "Remove all deploy directories."
  rm -rf ${PWD}/deploy-*
  exit 0
fi

if [ "$1" = "clean-cache" ]; then
  echo "Remove all cache build outputs."
  echo rm -rf ${HOME}/.maum-build
  rm -rf ${HOME}/.maum-build/*
  exit 0
fi

function clean_build_dir() {
  echo "Remove all build directories named as $1"
  find ./ -type d -a -name "$1*" | xargs rm -rf
}

if [ "$1" = "clean-cmake" ]; then
  clean_build_dir build-debug
  clean_build_dir build-deploy-debug
  exit 0
fi


GLOB_BUILD_DIR=${HOME}/.maum-build

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set centos as default."
  OS=centos
fi

if [ "${OS}" = "centos" ]; then
  __CC=/usr/bin/gcc
  __CXX=/usr/bin/g++
  CMAKE=/usr/bin/cmake3
else
  __CC=/usr/bin/gcc-4.8
  __CXX=/usr/bin/g++-4.8
  CMAKE=/usr/bin/cmake
fi


IS_TAR=0
if [ "$1" = "tar" ]; then
   temp_dir=$(mktemp -d ${PWD}/deploy-XXXXXX)
   export MAUM_ROOT=${temp_dir}/maum
   echo "temp dir ${MAUM_ROOT} created"
   IS_TAR=1
   export MAUM_BUILD_DEPLOY=true
else
  export MAUM_ROOT=$1
fi

test -d ${MAUM_ROOT} || mkdir -p ${MAUM_ROOT}


if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

## argument parameters
shift

TARGETS=()
if [ "$#" = 0 ]; then
  TARGETS+=('cek')
  TARGETS+=('mapauthlgu')
elif [ "$1" = "all" ]; then
  TARGETS+=('cek')
  TARGETS+=('mapauthlgu')
else
  TARGETS=("${@:1}")
fi


if [ ${IS_TAR} = 1 ]; then
  TARGETS+=('tar')
fi

echo "Build targets are ${TARGETS[*]}"

IMAGE_NAME=all


maum_root=${MAUM_ROOT}

REPO_ROOT=$(pwd)
# repo root is used in submodule build scirpt
export REPO_ROOT

APPS_ROOT=${maum_root}/apps

function make_folder() {
  test -d ${APPS_ROOT}/$1 || mkdir -p ${APPS_ROOT}/$1
}

LAST_OUT=.last_build_outdir && [[ "${IS_TAR}" == "1" ]] && LAST_OUT=.last_deploy_outdir

if [ -f ${LAST_OUT} ]; then
  last_root=$(cat ${LAST_OUT})

  if [ "${last_root}" != "${maum_root}" ]; then
    build_base="build-debug" && [[ "${IS_TAR}" = "1" ]] && build_base="build-deploy-debug"

    echo "clean last ${build_base} directories"
    clean_build_dir ${build_base}
  fi
fi
echo ${maum_root} > ${LAST_OUT}

# extension server distributions jar
function build_cek() {
  local src_dir=${REPO_ROOT}/external/cek-extension
  local build_dir=${src_dir}/build/libs
  local deploy_dir=${maum_root}/lib
    (cd ${src_dir};
   ./gradlew clean;
   ./gradlew build task build;
   cp ${build_dir}/cek-extension-1.0.0.jar ${deploy_dir}/;)
}

# map-auth server distributions jar
function build_map_auth_lgu() {
  local src_dir=${REPO_ROOT}/external/map-auth-lgu
  local build_dir=${src_dir}/build/install/map-auth-lgu-shadow/lib
  local deploy_dir=${maum_root}/lib
    (cd ${src_dir};
   ./gradlew clean;
   ./gradlew installShadowDist;
   cp ${build_dir}/map-auth-lgu-1.0.0-all.jar ${deploy_dir}/;)
}

function make_tar() {
  upperdir=$(dirname ${maum_root})
  tardir=$(basename ${maum_root})

  tag=$(git describe --abbrev=7 --tags)
  tarfile=m2u-${tag}-${IMAGE_NAME}-${OS}.tar.gz

  echo "------------------------------------"
  echo "Generating TAR ${tarfile} ..."
  echo "------------------------------------"

  GZIP=-1 tar -zcf ${upperdir}/${tarfile} -C ${upperdir} ${tardir} \
    --exclude "${tardir}/workspace/stt-training/*" \
    --exclude "${tardir}/workspace/classifier/*" \
    --exclude "${tardir}/resources/*" \
    --exclude "${tardir}/include/*.h" \
    --exclude "${tardir}/include/*.cc" \
    --exclude "${tardir}/lib/*.a" \
    --exclude "${tardir}/logs/*" \
    --exclude "${tardir}/trained/stt/*" \
    --exclude "${tardir}/trained/hmd/*" \
    --exclude "${tardir}/trained/classifier/*" \
    --exclude "${tardir}/share/cmake/*" \
    --exclude "${tardir}/apps/*/node_modules/*.o" \
    --exclude "${tardir}/apps/*/node_modules/*.c" \
    --exclude "${tardir}/apps/*/node_modules/*.cpp" \
    --exclude "${tardir}/apps/*/node_modules/*.cc" \
    --exclude "${tardir}/apps/*/node_modules/*.h" \
    --exclude "${tardir}/apps/*/node_modules/*.hh" \
    --exclude "${tardir}/apps/*/node_modules/*.hpp" \
    --exclude "${tardir}/apps/*/node_modules/*.in" \
    --exclude "${tardir}/apps/*/node_modules/*.am" \
    --exclude "${tardir}/apps/*/node_modules/*.ac" \
    --exclude "${tardir}/apps/*/node_modules/*.go" \
    --exclude "${tardir}/apps/*/node_modules/*.a" \
    --exclude "${tardir}/apps/*/node_modules/*.S" \
    --exclude "${tardir}/apps/*/node_modules/*.pas" \
    --exclude "${tardir}/apps/*/node_modules/*.sln" \
    --exclude "${tardir}/apps/*/node_modules/*.vcxproj" \
    --exclude "${tardir}/apps/*/node_modules/*.vcproj" \
    --exclude "${tardir}/apps/*/node_modules/*SConscript" \
    --exclude "${tardir}/apps/*/node_modules/*configure" \
    --exclude "${tardir}/apps/*/node_modules/grpc/third_party/*" \
    --exclude "${tardir}/bin/protoc" \
    --exclude "${tardir}/bin/grpc_*_plugin" \
    --exclude "${tardir}/*built" \
    --exclude "${tardir}/*extracted" \
    --exclude "${tardir}/*installed"

  echo m2u-${tag} > ${maum_root}/etc/revision
  test -d ${REPO_ROOT}/out || mkdir -p ${REPO_ROOT}/out
  mv ${upperdir}/${tarfile} ${REPO_ROOT}/out
  echo "------------------------------------"
  echo "TAR file out/${tarfile} created."
  echo "------------------------------------"

  # make_all_res_tar
  echo "please clean TEMP DIR : ${upperdir}"
}

### BUILD MAIN

for t in "${TARGETS[@]}"
do
  case "$t" in
    mapauthlgu) build_map_auth_lgu;;
    cek) build_cek;;
    tar) make_tar;;
    *) echo "INVALID TARGET $t";;
  esac
done
